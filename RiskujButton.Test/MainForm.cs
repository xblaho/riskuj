﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace SerialTest
{
    public partial class frmMain : Form
    {
        private RiskujButton.RiskujButton m_sp;

        public frmMain()
        {
            InitializeComponent();
            m_sp = new RiskujButton.RiskujButton(this);
            m_sp.ButtonPressed += sp_ButtonPressed;
            m_sp.PinChanged += M_sp_PinChanged;
        }

        private void M_sp_PinChanged(object sender, bool CDState, bool dsrState, bool rtsState)
        {
            if (InvokeRequired)
                Invoke(new RiskujButton.RiskujButton.PinChangedEventHandler(M_sp_PinChanged), sender, CDState, dsrState, rtsState);
            else
                listBox.Items.Add($"B1: {CDState}; B2: {dsrState}; B3: {rtsState}");
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            m_sp.AutoDetectAndOpen();
        }

        private void sp_ButtonPressed(object sender, int buttonNo)
        {
            lblCDState.Text = "Button1: " + (buttonNo==1 ? "on" : "off");
            lblDsrState.Text = "Button2: " + (buttonNo == 2 ? "on" : "off");
            lblCtsState.Text = "Button3: " + (buttonNo == 3 ? "on" : "off");
            listBox.Items.Add(buttonNo);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            m_sp.Close();
        }

        private void btnAutoDetect_Click(object sender, EventArgs e)
        {
            m_sp.AutoDetect();
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            listBox.Items.Clear();
        }
    }
}
