﻿namespace SerialTest
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblCtsState = new System.Windows.Forms.Label();
            this.lblDsrState = new System.Windows.Forms.Label();
            this.lblCDState = new System.Windows.Forms.Label();
            this.listBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(12, 12);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(128, 63);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(146, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(143, 63);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCtsState
            // 
            this.lblCtsState.AutoSize = true;
            this.lblCtsState.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCtsState.Location = new System.Drawing.Point(-1, 224);
            this.lblCtsState.Name = "lblCtsState";
            this.lblCtsState.Size = new System.Drawing.Size(374, 73);
            this.lblCtsState.TabIndex = 3;
            this.lblCtsState.Text = "Button3: off";
            // 
            // lblDsrState
            // 
            this.lblDsrState.AutoSize = true;
            this.lblDsrState.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDsrState.Location = new System.Drawing.Point(0, 151);
            this.lblDsrState.Name = "lblDsrState";
            this.lblDsrState.Size = new System.Drawing.Size(374, 73);
            this.lblDsrState.TabIndex = 4;
            this.lblDsrState.Text = "Button2: off";
            // 
            // lblCDState
            // 
            this.lblCDState.AutoSize = true;
            this.lblCDState.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCDState.Location = new System.Drawing.Point(0, 78);
            this.lblCDState.Name = "lblCDState";
            this.lblCDState.Size = new System.Drawing.Size(374, 73);
            this.lblCDState.TabIndex = 5;
            this.lblCDState.Text = "Button1: off";
            // 
            // listBox
            // 
            this.listBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox.FormattingEnabled = true;
            this.listBox.Location = new System.Drawing.Point(415, 12);
            this.listBox.Name = "listBox";
            this.listBox.Size = new System.Drawing.Size(218, 316);
            this.listBox.TabIndex = 6;
            this.listBox.DoubleClick += new System.EventHandler(this.listBox_DoubleClick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 338);
            this.Controls.Add(this.listBox);
            this.Controls.Add(this.lblCDState);
            this.Controls.Add(this.lblDsrState);
            this.Controls.Add(this.lblCtsState);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOpen);
            this.Name = "frmMain";
            this.Text = "SerialTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblCtsState;
        private System.Windows.Forms.Label lblDsrState;
        private System.Windows.Forms.Label lblCDState;
        private System.Windows.Forms.ListBox listBox;
    }
}

