﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace RiskujButton
{
    internal partial class AutodetectForm : Form
    {
        private BackgroundWorker m_bw;
        public AutodetectForm(BackgroundWorker bw)
        {
            InitializeComponent();
            m_bw = bw;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            CancelAutoDetectButton.Enabled = false;
            m_bw.CancelAsync();
        }
    }
}
