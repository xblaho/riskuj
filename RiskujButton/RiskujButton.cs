﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RiskujButton
{
    public class RiskujButton
    {
        private SerialPort m_port;
        private System.Windows.Forms.Control m_owner;
        private bool m_lastCDState;
        private bool m_lastDsrState;
        private bool m_lastCtsState;

        public string PortName { get; set; }

        public RiskujButton(System.Windows.Forms.Control owner)
        {
            m_owner = owner;
        }

        public bool Open()
        {
            if (m_port != null)
                Close();
            try
            {
                m_port = new SerialPort(PortName);
                m_port.Open();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
                m_port = null;
                return false;
            }
            m_port.PinChanged += port_PinChanged;
            m_port.RtsEnable = true;
            m_port.DtrEnable = true;
            return true;
        }

        public void AutoDetectAndOpen()
        {
            AutoDetect();
            if (!String.IsNullOrEmpty(PortName))
                Open();
        }

        public bool Close()
        {
            if (m_port == null)
                return true;
            try
            {
                if (m_port.IsOpen)
                    m_port.Close();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
                return false;
            }
            m_port = null;
            return true;
        }

        private AutodetectForm adForm;
        private BackgroundWorker bw;
        public void AutoDetect()
        {
            PortName = null;
            var ports = SerialPort.GetPortNames();
            if (ports.Length == 0)
            {
                System.Windows.Forms.MessageBox.Show("No serial ports found.", "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return;
            }
            bw = new BackgroundWorker();
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerAsync(ports);
            adForm = new AutodetectForm(bw);
            adForm.ShowDialog(m_owner);
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var ports = (string[])(e.Argument);
            var i = 0;
            while (!bw.CancellationPending)
            {
                SerialPort p = null;
                try
                {
                    SetAdFormLabel(ports[i]);
                    p = new SerialPort(ports[i]);
                    p.Open();
                    p.RtsEnable = true;
                    p.DtrEnable = true;
                    for (var c = 0; c < 20; c++)
                    {
                        SetIcons(p.CDHolding, p.DsrHolding, p.CtsHolding);
                        if (p.CDHolding && p.DsrHolding && p.CtsHolding)
                        {
                            e.Result = ports[i];
                            return;
                        }
                        Thread.Sleep(100);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("RiskujButton: " + ex.Message);
                }
                finally
                {
                    if (p.IsOpen)
                        p.Close();
                }
                i++;
                if (i >= ports.Count())
                    i = 0;
            }
        }

        internal delegate void SetLabelDelegate(string text);
        internal void SetAdFormLabel(string text)
        {
            if (m_owner.InvokeRequired)
                m_owner.Invoke(new SetLabelDelegate(SetAdFormLabel), text);
            else
                adForm.label.Text = text;
        }

        internal delegate void SetIconsDelegate(bool state1, bool state2, bool state3);
        internal void SetIcons(bool state1, bool state2, bool state3)
        {
            if (m_owner.InvokeRequired)
                m_owner.Invoke(new SetIconsDelegate(SetIcons), state1, state2, state3);
            else
            {
                adForm.pictureBox1.Image = state1 ? Properties.Resources.redon : Properties.Resources.redoff;
                adForm.pictureBox2.Image = state2 ? Properties.Resources.redon : Properties.Resources.redoff;
                adForm.pictureBox3.Image = state3 ? Properties.Resources.redon : Properties.Resources.redoff;
            }
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            adForm.Close();
            if (e.Cancelled)
                return;
            PortName = (string)e.Result;
        }

        private void port_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            if (!m_port.IsOpen)
                return;
            PinChanged?.Invoke(sender, m_port.CDHolding, m_port.DsrHolding, m_port.CtsHolding);
            if (m_owner.InvokeRequired)
                m_owner.Invoke(new SerialPinChangedEventHandler(port_PinChanged), sender, e);
            else
            {
                try
                {
                    var num = 0;
                    if (m_port.CDHolding)
                    {
                        if (m_lastCDState)
                            return;
                        num = 1;
                    }
                    else
                    {
                        if (m_port.DsrHolding)
                        {
                            if (m_lastDsrState)
                                return;
                            num = 2;
                        }
                        else
                        {
                            if (m_port.CtsHolding)
                            {
                                if (m_lastCtsState)
                                    return;
                                num = 3;
                            }
                        }
                    }
                    ButtonPressed?.Invoke(this, num);
                }
                finally
                {
                    m_lastCDState = m_port.CDHolding;
                    m_lastDsrState = m_port.DsrHolding;
                    m_lastCtsState = m_port.CtsHolding;
                }
            }
        }

        public delegate void ButtonPressedDelegate(object sender, int buttonNo);
        public event ButtonPressedDelegate ButtonPressed;

        public delegate void PinChangedEventHandler(object sender, bool CDState, bool dsrState, bool rtsState);
        public event PinChangedEventHandler PinChanged;
    }
}
