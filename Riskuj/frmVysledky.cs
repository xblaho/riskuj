﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Riskuj
{
    public partial class frmVysledky : Form
    {
        public frmVysledky()
        {
            InitializeComponent();
        }

        Stopwatch stopky = new Stopwatch();

        private int Castka = 0;
        private int tym1Castka = 0;
        private int tym2Castka = 0;
        private int tym3Castka = 0;
        private bool DruheKolo = false;
        private bool TretiKolo = false;
        private bool JizSpusteneDruheKolo = false;
        private bool JizSpusteneTretiKolo = false;
        private int CasNaDruheKolo = 15;

        public int GetSetCastka
        {
            get { return Castka; }
            set { Castka = value; }
        }

        public bool GetSetDruheKolo
        {
            get { return DruheKolo; }
            set { DruheKolo = value; }
        }

        public bool GetSetTretiKolo
        {
            get { return TretiKolo; }
            set { TretiKolo = value; }
        }

        public int GetSetCas
        {
            get { return CasNaDruheKolo; }
            set { CasNaDruheKolo = value; }
        }

        private void lCastka_Click(object sender, EventArgs e)
        {
            Castka = Castka * (-1);

            lCastka.Text = Castka.ToString();
        }

        private void frmVysledky_Load(object sender, EventArgs e)
        {
             
        }

        private void lTym1_Click(object sender, EventArgs e)
        {
            tym1Castka = tym1Castka + Castka;
            lTym1.Text = tym1Castka.ToString();

            if (Castka > 0)
            {
                Castka = 0;
                lCastka.Text = Castka.ToString();
            }
            else
            {
                Castka = Castka * (-1);
                lCastka.Text = Castka.ToString();
            }

            if (TretiKolo)
            {
                if (!JizSpusteneTretiKolo)
                {
                    JizSpusteneTretiKolo = true;

                    if (tym1Castka <= 0)
                    {
                        tym1Castka = 500;
                        lTym1.Text = tym1Castka.ToString();
                    }

                    if (tym2Castka <= 0)
                    {
                        tym2Castka = 500;
                        lTym2.Text = tym2Castka.ToString();
                    }

                    if (tym3Castka <= 0)
                    {
                        tym3Castka = 500;
                        lTym3.Text = tym3Castka.ToString();
                    }
                }
            }
        }

        private void frmVysledky_Activated(object sender, EventArgs e)
        {
            lCastka.Text = Castka.ToString();

            if (DruheKolo)
            {
                if (!JizSpusteneDruheKolo)
                {
                    lInfo.Text = "< časový limit je " + CasNaDruheKolo.ToString() + " minut >";
                    JizSpusteneDruheKolo = true;

                    timerVysledky.Enabled = true;
                    stopky.Start();
                }
            }

            if (TretiKolo)
            {
                if (!JizSpusteneTretiKolo)
                {
                    timerVysledky.Enabled = false;

                    lInfo.Text = "< bez časového limitu >";
                    lCas.Text = "00:00";
                }
            }
        }

        private void lTym2_Click(object sender, EventArgs e)
        {
            tym2Castka = tym2Castka + Castka;
            lTym2.Text = tym2Castka.ToString();

            if (Castka > 0)
            {
                Castka = 0;
                lCastka.Text = Castka.ToString();
            }
            else
            {
                Castka = Castka * (-1);
                lCastka.Text = Castka.ToString();
            }

            if (TretiKolo)
            {
                if (!JizSpusteneTretiKolo)
                {
                    JizSpusteneTretiKolo = true;

                    if (tym1Castka <= 0)
                    {
                        tym1Castka = 500;
                        lTym1.Text = tym1Castka.ToString();
                    }

                    if (tym2Castka <= 0)
                    {
                        tym2Castka = 500;
                        lTym2.Text = tym2Castka.ToString();
                    }

                    if (tym3Castka <= 0)
                    {
                        tym3Castka = 500;
                        lTym3.Text = tym3Castka.ToString();
                    }
                }
            }
        }

        private void lTym3_Click(object sender, EventArgs e)
        {
            tym3Castka = tym3Castka + Castka;
            lTym3.Text = tym3Castka.ToString();

            if (Castka > 0)
            {
                Castka = 0;
                lCastka.Text = Castka.ToString();
            }
            else
            {
                Castka = Castka * (-1);
                lCastka.Text = Castka.ToString();
            }

            if (TretiKolo)
            {
                if (!JizSpusteneTretiKolo)
                {
                    JizSpusteneTretiKolo = true;

                    if (tym1Castka <= 0)
                    {
                        tym1Castka = 500;
                        lTym1.Text = tym1Castka.ToString();
                    }

                    if (tym2Castka <= 0)
                    {
                        tym2Castka = 500;
                        lTym2.Text = tym2Castka.ToString();
                    }

                    if (tym3Castka <= 0)
                    {
                        tym3Castka = 500;
                        lTym3.Text = tym3Castka.ToString();
                    }
                }
            }
        }

        private string formatCas(int hodnota)
        {
            string formatovanyCas = "";

            if (hodnota < 10) formatovanyCas = "0" + hodnota.ToString();
            else formatovanyCas = hodnota.ToString();

            return formatovanyCas;
        }

        private void timerVysledky_Tick(object sender, EventArgs e)
        {
            lCas.Text = formatCas(stopky.Elapsed.Minutes) + ":" + formatCas(stopky.Elapsed.Seconds);

            if (stopky.Elapsed.Minutes >= CasNaDruheKolo)
            {
                timerVysledky.Enabled = false;
                stopky.Stop();

                lInfo.Text = "< poslední otázka druhého kola >";
                lCas.Text = "Čas vypršel";
            }
        }

        private void lTym1Text_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.GetSetCastka = tym1Castka;

            formCastka.ShowDialog();

            tym1Castka = formCastka.GetSetCastka;
            lTym1.Text = tym1Castka.ToString();
        }

        private void lTym2Text_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.GetSetCastka = tym2Castka;

            formCastka.ShowDialog();

            tym2Castka = formCastka.GetSetCastka;
            lTym2.Text = tym2Castka.ToString();
        }

        private void lTym3Text_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.GetSetCastka = tym3Castka;

            formCastka.ShowDialog();

            tym3Castka = formCastka.GetSetCastka;
            lTym3.Text = tym3Castka.ToString();
        }
    }
}
