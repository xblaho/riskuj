﻿namespace Riskuj
{
    partial class frmVysledky
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.lCastka = new System.Windows.Forms.Label();
            this.lTym1Text = new System.Windows.Forms.Label();
            this.lTym1 = new System.Windows.Forms.Label();
            this.lTym2Text = new System.Windows.Forms.Label();
            this.lTym2 = new System.Windows.Forms.Label();
            this.lTym3Text = new System.Windows.Forms.Label();
            this.lTym3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lCas = new System.Windows.Forms.Label();
            this.timerVysledky = new System.Windows.Forms.Timer(this.components);
            this.lInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.Yellow;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Částka:";
            // 
            // lCastka
            // 
            this.lCastka.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCastka.ForeColor = System.Drawing.Color.Yellow;
            this.lCastka.Location = new System.Drawing.Point(104, 9);
            this.lCastka.Name = "lCastka";
            this.lCastka.Size = new System.Drawing.Size(168, 26);
            this.lCastka.TabIndex = 1;
            this.lCastka.Text = "0";
            this.lCastka.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lCastka.Click += new System.EventHandler(this.lCastka_Click);
            // 
            // lTym1Text
            // 
            this.lTym1Text.AutoSize = true;
            this.lTym1Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym1Text.ForeColor = System.Drawing.Color.Yellow;
            this.lTym1Text.Location = new System.Drawing.Point(12, 70);
            this.lTym1Text.Name = "lTym1Text";
            this.lTym1Text.Size = new System.Drawing.Size(78, 26);
            this.lTym1Text.TabIndex = 2;
            this.lTym1Text.Text = "Tým 1:";
            this.lTym1Text.Click += new System.EventHandler(this.lTym1Text_Click);
            // 
            // lTym1
            // 
            this.lTym1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym1.ForeColor = System.Drawing.Color.Yellow;
            this.lTym1.Location = new System.Drawing.Point(109, 70);
            this.lTym1.Name = "lTym1";
            this.lTym1.Size = new System.Drawing.Size(163, 26);
            this.lTym1.TabIndex = 3;
            this.lTym1.Text = "0";
            this.lTym1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lTym1.Click += new System.EventHandler(this.lTym1_Click);
            // 
            // lTym2Text
            // 
            this.lTym2Text.AutoSize = true;
            this.lTym2Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym2Text.ForeColor = System.Drawing.Color.Yellow;
            this.lTym2Text.Location = new System.Drawing.Point(12, 115);
            this.lTym2Text.Name = "lTym2Text";
            this.lTym2Text.Size = new System.Drawing.Size(78, 26);
            this.lTym2Text.TabIndex = 4;
            this.lTym2Text.Text = "Tým 2:";
            this.lTym2Text.Click += new System.EventHandler(this.lTym2Text_Click);
            // 
            // lTym2
            // 
            this.lTym2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym2.ForeColor = System.Drawing.Color.Yellow;
            this.lTym2.Location = new System.Drawing.Point(114, 115);
            this.lTym2.Name = "lTym2";
            this.lTym2.Size = new System.Drawing.Size(158, 26);
            this.lTym2.TabIndex = 5;
            this.lTym2.Text = "0";
            this.lTym2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lTym2.Click += new System.EventHandler(this.lTym2_Click);
            // 
            // lTym3Text
            // 
            this.lTym3Text.AutoSize = true;
            this.lTym3Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym3Text.ForeColor = System.Drawing.Color.Yellow;
            this.lTym3Text.Location = new System.Drawing.Point(12, 162);
            this.lTym3Text.Name = "lTym3Text";
            this.lTym3Text.Size = new System.Drawing.Size(78, 26);
            this.lTym3Text.TabIndex = 6;
            this.lTym3Text.Text = "Tým 3:";
            this.lTym3Text.Click += new System.EventHandler(this.lTym3Text_Click);
            // 
            // lTym3
            // 
            this.lTym3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTym3.ForeColor = System.Drawing.Color.Yellow;
            this.lTym3.Location = new System.Drawing.Point(119, 162);
            this.lTym3.Name = "lTym3";
            this.lTym3.Size = new System.Drawing.Size(153, 26);
            this.lTym3.TabIndex = 7;
            this.lTym3.Text = "0";
            this.lTym3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lTym3.Click += new System.EventHandler(this.lTym3_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(2, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(280, 26);
            this.label2.TabIndex = 8;
            this.label2.Text = "______________________";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(2, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(280, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "______________________";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.Color.Yellow;
            this.label6.Location = new System.Drawing.Point(12, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 26);
            this.label6.TabIndex = 10;
            this.label6.Text = "Čas:";
            // 
            // lCas
            // 
            this.lCas.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCas.ForeColor = System.Drawing.Color.Yellow;
            this.lCas.Location = new System.Drawing.Point(104, 243);
            this.lCas.Name = "lCas";
            this.lCas.Size = new System.Drawing.Size(168, 26);
            this.lCas.TabIndex = 11;
            this.lCas.Text = "00:00";
            this.lCas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timerVysledky
            // 
            this.timerVysledky.Tick += new System.EventHandler(this.timerVysledky_Tick);
            // 
            // lInfo
            // 
            this.lInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lInfo.ForeColor = System.Drawing.Color.Yellow;
            this.lInfo.Location = new System.Drawing.Point(2, 214);
            this.lInfo.Name = "lInfo";
            this.lInfo.Size = new System.Drawing.Size(280, 26);
            this.lInfo.TabIndex = 12;
            this.lInfo.Text = "< bez časového limitu >";
            this.lInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmVysledky
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(284, 279);
            this.ControlBox = false;
            this.Controls.Add(this.lInfo);
            this.Controls.Add(this.lCas);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lTym3);
            this.Controls.Add(this.lTym3Text);
            this.Controls.Add(this.lTym2);
            this.Controls.Add(this.lTym2Text);
            this.Controls.Add(this.lTym1);
            this.Controls.Add(this.lTym1Text);
            this.Controls.Add(this.lCastka);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmVysledky";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Výsledky";
            this.Activated += new System.EventHandler(this.frmVysledky_Activated);
            this.Load += new System.EventHandler(this.frmVysledky_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lCastka;
        private System.Windows.Forms.Label lTym1Text;
        private System.Windows.Forms.Label lTym1;
        private System.Windows.Forms.Label lTym2Text;
        private System.Windows.Forms.Label lTym2;
        private System.Windows.Forms.Label lTym3Text;
        private System.Windows.Forms.Label lTym3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lCas;
        private System.Windows.Forms.Timer timerVysledky;
        private System.Windows.Forms.Label lInfo;
    }
}