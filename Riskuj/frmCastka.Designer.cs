﻿namespace Riskuj
{
    partial class frmCastka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nmCastka = new System.Windows.Forms.NumericUpDown();
            this.btnOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nmCastka)).BeginInit();
            this.SuspendLayout();
            // 
            // nmCastka
            // 
            this.nmCastka.BackColor = System.Drawing.Color.Blue;
            this.nmCastka.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nmCastka.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nmCastka.ForeColor = System.Drawing.Color.Yellow;
            this.nmCastka.Location = new System.Drawing.Point(23, 24);
            this.nmCastka.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.nmCastka.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.nmCastka.Name = "nmCastka";
            this.nmCastka.Size = new System.Drawing.Size(273, 37);
            this.nmCastka.TabIndex = 0;
            this.nmCastka.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nmCastka.ValueChanged += new System.EventHandler(this.nmCastka_ValueChanged);
            this.nmCastka.Enter += new System.EventHandler(this.nmCastka_Enter);
            // 
            // btnOK
            // 
            this.btnOK.ForeColor = System.Drawing.Color.Black;
            this.btnOK.Location = new System.Drawing.Point(115, 85);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // frmCastka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(308, 129);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.nmCastka);
            this.ForeColor = System.Drawing.Color.Yellow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCastka";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vsadit částku";
            this.Shown += new System.EventHandler(this.frmCastka_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.nmCastka)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nmCastka;
        private System.Windows.Forms.Button btnOK;
    }
}