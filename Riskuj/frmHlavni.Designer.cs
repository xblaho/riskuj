﻿namespace Riskuj
{
    partial class frmHlavni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelKolo1 = new System.Windows.Forms.Panel();
            this.btnTema65000 = new C1.Win.C1Input.C1Button();
            this.btnTema55000 = new C1.Win.C1Input.C1Button();
            this.btnTema45000 = new C1.Win.C1Input.C1Button();
            this.btnTema35000 = new C1.Win.C1Input.C1Button();
            this.btnTema25000 = new C1.Win.C1Input.C1Button();
            this.btnTema15000 = new C1.Win.C1Input.C1Button();
            this.btnTema64000 = new C1.Win.C1Input.C1Button();
            this.btnTema54000 = new C1.Win.C1Input.C1Button();
            this.btnTema44000 = new C1.Win.C1Input.C1Button();
            this.btnTema34000 = new C1.Win.C1Input.C1Button();
            this.btnTema24000 = new C1.Win.C1Input.C1Button();
            this.btnTema14000 = new C1.Win.C1Input.C1Button();
            this.btnTema63000 = new C1.Win.C1Input.C1Button();
            this.btnTema53000 = new C1.Win.C1Input.C1Button();
            this.btnTema43000 = new C1.Win.C1Input.C1Button();
            this.btnTema33000 = new C1.Win.C1Input.C1Button();
            this.btnTema23000 = new C1.Win.C1Input.C1Button();
            this.btnTema13000 = new C1.Win.C1Input.C1Button();
            this.btnTema62000 = new C1.Win.C1Input.C1Button();
            this.btnTema52000 = new C1.Win.C1Input.C1Button();
            this.btnTema42000 = new C1.Win.C1Input.C1Button();
            this.btnTema32000 = new C1.Win.C1Input.C1Button();
            this.btnTema22000 = new C1.Win.C1Input.C1Button();
            this.btnTema12000 = new C1.Win.C1Input.C1Button();
            this.btnTema61000 = new C1.Win.C1Input.C1Button();
            this.btnTema51000 = new C1.Win.C1Input.C1Button();
            this.btnTema41000 = new C1.Win.C1Input.C1Button();
            this.btnTema31000 = new C1.Win.C1Input.C1Button();
            this.btnTema21000 = new C1.Win.C1Input.C1Button();
            this.btnTema11000 = new C1.Win.C1Input.C1Button();
            this.btnTema6Bonus = new C1.Win.C1Input.C1Button();
            this.btnTema5Bonus = new C1.Win.C1Input.C1Button();
            this.btnTema4Bonus = new C1.Win.C1Input.C1Button();
            this.btnTema3Bonus = new C1.Win.C1Input.C1Button();
            this.btnTema2Bonus = new C1.Win.C1Input.C1Button();
            this.btnTema1Bonus = new C1.Win.C1Input.C1Button();
            this.panelOtazka = new System.Windows.Forms.Panel();
            this.lOtazka = new System.Windows.Forms.Label();
            this.panelCas = new System.Windows.Forms.Panel();
            this.picture12 = new System.Windows.Forms.PictureBox();
            this.picture11 = new System.Windows.Forms.PictureBox();
            this.picture10 = new System.Windows.Forms.PictureBox();
            this.picture9 = new System.Windows.Forms.PictureBox();
            this.picture8 = new System.Windows.Forms.PictureBox();
            this.picture7 = new System.Windows.Forms.PictureBox();
            this.picture6 = new System.Windows.Forms.PictureBox();
            this.picture5 = new System.Windows.Forms.PictureBox();
            this.picture4 = new System.Windows.Forms.PictureBox();
            this.picture3 = new System.Windows.Forms.PictureBox();
            this.picture2 = new System.Windows.Forms.PictureBox();
            this.picture1 = new System.Windows.Forms.PictureBox();
            this.lCasNaOtázku = new System.Windows.Forms.Label();
            this.lPrihlasenyTym = new System.Windows.Forms.Label();
            this.panelKolo2 = new System.Windows.Forms.Panel();
            this.btn2Kolo36 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo30 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo24 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo18 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo12 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo6 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo35 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo29 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo23 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo17 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo11 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo5 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo34 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo28 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo22 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo16 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo10 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo4 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo33 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo27 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo21 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo15 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo9 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo3 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo32 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo26 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo20 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo14 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo8 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo2 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo31 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo25 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo19 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo13 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo7 = new C1.Win.C1Input.C1Button();
            this.btn2Kolo1 = new C1.Win.C1Input.C1Button();
            this.panelKonec = new System.Windows.Forms.Panel();
            this.lKonec = new System.Windows.Forms.Label();
            this.panelKolo3 = new System.Windows.Forms.Panel();
            this.btnRubl = new C1.Win.C1Input.C1Button();
            this.btnRupie = new C1.Win.C1Input.C1Button();
            this.btnEuro = new C1.Win.C1Input.C1Button();
            this.btnLibra = new C1.Win.C1Input.C1Button();
            this.btnDolar = new C1.Win.C1Input.C1Button();
            this.btnKoruna = new C1.Win.C1Input.C1Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.timerOdpoved = new System.Windows.Forms.Timer(this.components);
            this.panelKolo1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema65000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema55000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema45000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema35000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema25000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema15000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema64000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema54000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema44000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema34000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema24000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema14000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema63000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema53000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema43000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema33000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema23000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema13000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema62000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema52000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema42000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema32000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema22000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema12000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema61000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema51000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema41000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema31000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema21000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema11000)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema6Bonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema5Bonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema4Bonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema3Bonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema2Bonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema1Bonus)).BeginInit();
            this.panelOtazka.SuspendLayout();
            this.panelCas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            this.panelKolo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo1)).BeginInit();
            this.panelKonec.SuspendLayout();
            this.panelKolo3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRubl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRupie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEuro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLibra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDolar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKoruna)).BeginInit();
            this.SuspendLayout();
            // 
            // panelKolo1
            // 
            this.panelKolo1.Controls.Add(this.btnTema65000);
            this.panelKolo1.Controls.Add(this.btnTema55000);
            this.panelKolo1.Controls.Add(this.btnTema45000);
            this.panelKolo1.Controls.Add(this.btnTema35000);
            this.panelKolo1.Controls.Add(this.btnTema25000);
            this.panelKolo1.Controls.Add(this.btnTema15000);
            this.panelKolo1.Controls.Add(this.btnTema64000);
            this.panelKolo1.Controls.Add(this.btnTema54000);
            this.panelKolo1.Controls.Add(this.btnTema44000);
            this.panelKolo1.Controls.Add(this.btnTema34000);
            this.panelKolo1.Controls.Add(this.btnTema24000);
            this.panelKolo1.Controls.Add(this.btnTema14000);
            this.panelKolo1.Controls.Add(this.btnTema63000);
            this.panelKolo1.Controls.Add(this.btnTema53000);
            this.panelKolo1.Controls.Add(this.btnTema43000);
            this.panelKolo1.Controls.Add(this.btnTema33000);
            this.panelKolo1.Controls.Add(this.btnTema23000);
            this.panelKolo1.Controls.Add(this.btnTema13000);
            this.panelKolo1.Controls.Add(this.btnTema62000);
            this.panelKolo1.Controls.Add(this.btnTema52000);
            this.panelKolo1.Controls.Add(this.btnTema42000);
            this.panelKolo1.Controls.Add(this.btnTema32000);
            this.panelKolo1.Controls.Add(this.btnTema22000);
            this.panelKolo1.Controls.Add(this.btnTema12000);
            this.panelKolo1.Controls.Add(this.btnTema61000);
            this.panelKolo1.Controls.Add(this.btnTema51000);
            this.panelKolo1.Controls.Add(this.btnTema41000);
            this.panelKolo1.Controls.Add(this.btnTema31000);
            this.panelKolo1.Controls.Add(this.btnTema21000);
            this.panelKolo1.Controls.Add(this.btnTema11000);
            this.panelKolo1.Controls.Add(this.btnTema6Bonus);
            this.panelKolo1.Controls.Add(this.btnTema5Bonus);
            this.panelKolo1.Controls.Add(this.btnTema4Bonus);
            this.panelKolo1.Controls.Add(this.btnTema3Bonus);
            this.panelKolo1.Controls.Add(this.btnTema2Bonus);
            this.panelKolo1.Controls.Add(this.btnTema1Bonus);
            this.panelKolo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelKolo1.Location = new System.Drawing.Point(0, 0);
            this.panelKolo1.Name = "panelKolo1";
            this.panelKolo1.Size = new System.Drawing.Size(934, 636);
            this.panelKolo1.TabIndex = 36;
            // 
            // btnTema65000
            // 
            this.btnTema65000.BackColor = System.Drawing.Color.Blue;
            this.btnTema65000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema65000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema65000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema65000.ForeColor = System.Drawing.Color.White;
            this.btnTema65000.Location = new System.Drawing.Point(782, 533);
            this.btnTema65000.Name = "btnTema65000";
            this.btnTema65000.Size = new System.Drawing.Size(150, 100);
            this.btnTema65000.TabIndex = 71;
            this.btnTema65000.Text = "5000";
            this.btnTema65000.UseVisualStyleBackColor = false;
            this.btnTema65000.Click += new System.EventHandler(this.btnTema65000_Click);
            // 
            // btnTema55000
            // 
            this.btnTema55000.BackColor = System.Drawing.Color.Blue;
            this.btnTema55000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema55000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema55000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema55000.ForeColor = System.Drawing.Color.White;
            this.btnTema55000.Location = new System.Drawing.Point(626, 533);
            this.btnTema55000.Name = "btnTema55000";
            this.btnTema55000.Size = new System.Drawing.Size(150, 100);
            this.btnTema55000.TabIndex = 70;
            this.btnTema55000.Text = "5000";
            this.btnTema55000.UseVisualStyleBackColor = false;
            this.btnTema55000.Click += new System.EventHandler(this.btnTema55000_Click);
            // 
            // btnTema45000
            // 
            this.btnTema45000.BackColor = System.Drawing.Color.Blue;
            this.btnTema45000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema45000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema45000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema45000.ForeColor = System.Drawing.Color.White;
            this.btnTema45000.Location = new System.Drawing.Point(470, 533);
            this.btnTema45000.Name = "btnTema45000";
            this.btnTema45000.Size = new System.Drawing.Size(150, 100);
            this.btnTema45000.TabIndex = 69;
            this.btnTema45000.Text = "5000";
            this.btnTema45000.UseVisualStyleBackColor = false;
            this.btnTema45000.Click += new System.EventHandler(this.btnTema45000_Click);
            // 
            // btnTema35000
            // 
            this.btnTema35000.BackColor = System.Drawing.Color.Blue;
            this.btnTema35000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema35000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema35000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema35000.ForeColor = System.Drawing.Color.White;
            this.btnTema35000.Location = new System.Drawing.Point(314, 533);
            this.btnTema35000.Name = "btnTema35000";
            this.btnTema35000.Size = new System.Drawing.Size(150, 100);
            this.btnTema35000.TabIndex = 68;
            this.btnTema35000.Text = "5000";
            this.btnTema35000.UseVisualStyleBackColor = false;
            this.btnTema35000.Click += new System.EventHandler(this.btnTema35000_Click);
            // 
            // btnTema25000
            // 
            this.btnTema25000.BackColor = System.Drawing.Color.Blue;
            this.btnTema25000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema25000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema25000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema25000.ForeColor = System.Drawing.Color.White;
            this.btnTema25000.Location = new System.Drawing.Point(158, 533);
            this.btnTema25000.Name = "btnTema25000";
            this.btnTema25000.Size = new System.Drawing.Size(150, 100);
            this.btnTema25000.TabIndex = 67;
            this.btnTema25000.Text = "5000";
            this.btnTema25000.UseVisualStyleBackColor = false;
            this.btnTema25000.Click += new System.EventHandler(this.btnTema25000_Click);
            // 
            // btnTema15000
            // 
            this.btnTema15000.BackColor = System.Drawing.Color.Blue;
            this.btnTema15000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema15000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema15000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema15000.ForeColor = System.Drawing.Color.White;
            this.btnTema15000.Location = new System.Drawing.Point(2, 533);
            this.btnTema15000.Name = "btnTema15000";
            this.btnTema15000.Size = new System.Drawing.Size(150, 100);
            this.btnTema15000.TabIndex = 66;
            this.btnTema15000.Text = "5000";
            this.btnTema15000.UseVisualStyleBackColor = false;
            this.btnTema15000.Click += new System.EventHandler(this.btnTema15000_Click);
            // 
            // btnTema64000
            // 
            this.btnTema64000.BackColor = System.Drawing.Color.Blue;
            this.btnTema64000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema64000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema64000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema64000.ForeColor = System.Drawing.Color.White;
            this.btnTema64000.Location = new System.Drawing.Point(782, 427);
            this.btnTema64000.Name = "btnTema64000";
            this.btnTema64000.Size = new System.Drawing.Size(150, 100);
            this.btnTema64000.TabIndex = 65;
            this.btnTema64000.Text = "4000";
            this.btnTema64000.UseVisualStyleBackColor = false;
            this.btnTema64000.Click += new System.EventHandler(this.btnTema64000_Click);
            // 
            // btnTema54000
            // 
            this.btnTema54000.BackColor = System.Drawing.Color.Blue;
            this.btnTema54000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema54000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema54000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema54000.ForeColor = System.Drawing.Color.White;
            this.btnTema54000.Location = new System.Drawing.Point(626, 427);
            this.btnTema54000.Name = "btnTema54000";
            this.btnTema54000.Size = new System.Drawing.Size(150, 100);
            this.btnTema54000.TabIndex = 64;
            this.btnTema54000.Text = "4000";
            this.btnTema54000.UseVisualStyleBackColor = false;
            this.btnTema54000.Click += new System.EventHandler(this.btnTema54000_Click);
            // 
            // btnTema44000
            // 
            this.btnTema44000.BackColor = System.Drawing.Color.Blue;
            this.btnTema44000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema44000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema44000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema44000.ForeColor = System.Drawing.Color.White;
            this.btnTema44000.Location = new System.Drawing.Point(470, 427);
            this.btnTema44000.Name = "btnTema44000";
            this.btnTema44000.Size = new System.Drawing.Size(150, 100);
            this.btnTema44000.TabIndex = 63;
            this.btnTema44000.Text = "4000";
            this.btnTema44000.UseVisualStyleBackColor = false;
            this.btnTema44000.Click += new System.EventHandler(this.btnTema44000_Click);
            // 
            // btnTema34000
            // 
            this.btnTema34000.BackColor = System.Drawing.Color.Blue;
            this.btnTema34000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema34000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema34000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema34000.ForeColor = System.Drawing.Color.White;
            this.btnTema34000.Location = new System.Drawing.Point(314, 427);
            this.btnTema34000.Name = "btnTema34000";
            this.btnTema34000.Size = new System.Drawing.Size(150, 100);
            this.btnTema34000.TabIndex = 62;
            this.btnTema34000.Text = "4000";
            this.btnTema34000.UseVisualStyleBackColor = false;
            this.btnTema34000.Click += new System.EventHandler(this.btnTema34000_Click);
            // 
            // btnTema24000
            // 
            this.btnTema24000.BackColor = System.Drawing.Color.Blue;
            this.btnTema24000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema24000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema24000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema24000.ForeColor = System.Drawing.Color.White;
            this.btnTema24000.Location = new System.Drawing.Point(158, 427);
            this.btnTema24000.Name = "btnTema24000";
            this.btnTema24000.Size = new System.Drawing.Size(150, 100);
            this.btnTema24000.TabIndex = 61;
            this.btnTema24000.Text = "4000";
            this.btnTema24000.UseVisualStyleBackColor = false;
            this.btnTema24000.Click += new System.EventHandler(this.btnTema24000_Click);
            // 
            // btnTema14000
            // 
            this.btnTema14000.BackColor = System.Drawing.Color.Blue;
            this.btnTema14000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema14000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema14000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema14000.ForeColor = System.Drawing.Color.White;
            this.btnTema14000.Location = new System.Drawing.Point(2, 427);
            this.btnTema14000.Name = "btnTema14000";
            this.btnTema14000.Size = new System.Drawing.Size(150, 100);
            this.btnTema14000.TabIndex = 60;
            this.btnTema14000.Text = "4000";
            this.btnTema14000.UseVisualStyleBackColor = false;
            this.btnTema14000.Click += new System.EventHandler(this.btnTema14000_Click);
            // 
            // btnTema63000
            // 
            this.btnTema63000.BackColor = System.Drawing.Color.Blue;
            this.btnTema63000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema63000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema63000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema63000.ForeColor = System.Drawing.Color.White;
            this.btnTema63000.Location = new System.Drawing.Point(782, 321);
            this.btnTema63000.Name = "btnTema63000";
            this.btnTema63000.Size = new System.Drawing.Size(150, 100);
            this.btnTema63000.TabIndex = 59;
            this.btnTema63000.Text = "3000";
            this.btnTema63000.UseVisualStyleBackColor = false;
            this.btnTema63000.Click += new System.EventHandler(this.btnTema63000_Click);
            // 
            // btnTema53000
            // 
            this.btnTema53000.BackColor = System.Drawing.Color.Blue;
            this.btnTema53000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema53000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema53000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema53000.ForeColor = System.Drawing.Color.White;
            this.btnTema53000.Location = new System.Drawing.Point(626, 321);
            this.btnTema53000.Name = "btnTema53000";
            this.btnTema53000.Size = new System.Drawing.Size(150, 100);
            this.btnTema53000.TabIndex = 58;
            this.btnTema53000.Text = "3000";
            this.btnTema53000.UseVisualStyleBackColor = false;
            this.btnTema53000.Click += new System.EventHandler(this.btnTema53000_Click);
            // 
            // btnTema43000
            // 
            this.btnTema43000.BackColor = System.Drawing.Color.Blue;
            this.btnTema43000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema43000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema43000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema43000.ForeColor = System.Drawing.Color.White;
            this.btnTema43000.Location = new System.Drawing.Point(470, 321);
            this.btnTema43000.Name = "btnTema43000";
            this.btnTema43000.Size = new System.Drawing.Size(150, 100);
            this.btnTema43000.TabIndex = 57;
            this.btnTema43000.Text = "3000";
            this.btnTema43000.UseVisualStyleBackColor = false;
            this.btnTema43000.Click += new System.EventHandler(this.btnTema43000_Click);
            // 
            // btnTema33000
            // 
            this.btnTema33000.BackColor = System.Drawing.Color.Blue;
            this.btnTema33000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema33000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema33000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema33000.ForeColor = System.Drawing.Color.White;
            this.btnTema33000.Location = new System.Drawing.Point(314, 321);
            this.btnTema33000.Name = "btnTema33000";
            this.btnTema33000.Size = new System.Drawing.Size(150, 100);
            this.btnTema33000.TabIndex = 56;
            this.btnTema33000.Text = "3000";
            this.btnTema33000.UseVisualStyleBackColor = false;
            this.btnTema33000.Click += new System.EventHandler(this.btnTema33000_Click);
            // 
            // btnTema23000
            // 
            this.btnTema23000.BackColor = System.Drawing.Color.Blue;
            this.btnTema23000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema23000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema23000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema23000.ForeColor = System.Drawing.Color.White;
            this.btnTema23000.Location = new System.Drawing.Point(158, 321);
            this.btnTema23000.Name = "btnTema23000";
            this.btnTema23000.Size = new System.Drawing.Size(150, 100);
            this.btnTema23000.TabIndex = 55;
            this.btnTema23000.Text = "3000";
            this.btnTema23000.UseVisualStyleBackColor = false;
            this.btnTema23000.Click += new System.EventHandler(this.btnTema23000_Click);
            // 
            // btnTema13000
            // 
            this.btnTema13000.BackColor = System.Drawing.Color.Blue;
            this.btnTema13000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema13000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema13000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema13000.ForeColor = System.Drawing.Color.White;
            this.btnTema13000.Location = new System.Drawing.Point(2, 321);
            this.btnTema13000.Name = "btnTema13000";
            this.btnTema13000.Size = new System.Drawing.Size(150, 100);
            this.btnTema13000.TabIndex = 54;
            this.btnTema13000.Text = "3000";
            this.btnTema13000.UseVisualStyleBackColor = false;
            this.btnTema13000.Click += new System.EventHandler(this.btnTema13000_Click);
            // 
            // btnTema62000
            // 
            this.btnTema62000.BackColor = System.Drawing.Color.Blue;
            this.btnTema62000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema62000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema62000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema62000.ForeColor = System.Drawing.Color.White;
            this.btnTema62000.Location = new System.Drawing.Point(782, 215);
            this.btnTema62000.Name = "btnTema62000";
            this.btnTema62000.Size = new System.Drawing.Size(150, 100);
            this.btnTema62000.TabIndex = 53;
            this.btnTema62000.Text = "2000";
            this.btnTema62000.UseVisualStyleBackColor = false;
            this.btnTema62000.Click += new System.EventHandler(this.btnTema62000_Click);
            // 
            // btnTema52000
            // 
            this.btnTema52000.BackColor = System.Drawing.Color.Blue;
            this.btnTema52000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema52000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema52000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema52000.ForeColor = System.Drawing.Color.White;
            this.btnTema52000.Location = new System.Drawing.Point(626, 215);
            this.btnTema52000.Name = "btnTema52000";
            this.btnTema52000.Size = new System.Drawing.Size(150, 100);
            this.btnTema52000.TabIndex = 52;
            this.btnTema52000.Text = "2000";
            this.btnTema52000.UseVisualStyleBackColor = false;
            this.btnTema52000.Click += new System.EventHandler(this.btnTema52000_Click);
            // 
            // btnTema42000
            // 
            this.btnTema42000.BackColor = System.Drawing.Color.Blue;
            this.btnTema42000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema42000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema42000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema42000.ForeColor = System.Drawing.Color.White;
            this.btnTema42000.Location = new System.Drawing.Point(470, 215);
            this.btnTema42000.Name = "btnTema42000";
            this.btnTema42000.Size = new System.Drawing.Size(150, 100);
            this.btnTema42000.TabIndex = 51;
            this.btnTema42000.Text = "2000";
            this.btnTema42000.UseVisualStyleBackColor = false;
            this.btnTema42000.Click += new System.EventHandler(this.btnTema42000_Click);
            // 
            // btnTema32000
            // 
            this.btnTema32000.BackColor = System.Drawing.Color.Blue;
            this.btnTema32000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema32000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema32000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema32000.ForeColor = System.Drawing.Color.White;
            this.btnTema32000.Location = new System.Drawing.Point(314, 215);
            this.btnTema32000.Name = "btnTema32000";
            this.btnTema32000.Size = new System.Drawing.Size(150, 100);
            this.btnTema32000.TabIndex = 50;
            this.btnTema32000.Text = "2000";
            this.btnTema32000.UseVisualStyleBackColor = false;
            this.btnTema32000.Click += new System.EventHandler(this.btnTema32000_Click);
            // 
            // btnTema22000
            // 
            this.btnTema22000.BackColor = System.Drawing.Color.Blue;
            this.btnTema22000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema22000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema22000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema22000.ForeColor = System.Drawing.Color.White;
            this.btnTema22000.Location = new System.Drawing.Point(158, 215);
            this.btnTema22000.Name = "btnTema22000";
            this.btnTema22000.Size = new System.Drawing.Size(150, 100);
            this.btnTema22000.TabIndex = 49;
            this.btnTema22000.Text = "2000";
            this.btnTema22000.UseVisualStyleBackColor = false;
            this.btnTema22000.Click += new System.EventHandler(this.btnTema22000_Click);
            // 
            // btnTema12000
            // 
            this.btnTema12000.BackColor = System.Drawing.Color.Blue;
            this.btnTema12000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema12000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema12000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema12000.ForeColor = System.Drawing.Color.White;
            this.btnTema12000.Location = new System.Drawing.Point(2, 215);
            this.btnTema12000.Name = "btnTema12000";
            this.btnTema12000.Size = new System.Drawing.Size(150, 100);
            this.btnTema12000.TabIndex = 48;
            this.btnTema12000.Text = "2000";
            this.btnTema12000.UseVisualStyleBackColor = false;
            this.btnTema12000.Click += new System.EventHandler(this.btnTema12000_Click);
            // 
            // btnTema61000
            // 
            this.btnTema61000.BackColor = System.Drawing.Color.Blue;
            this.btnTema61000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema61000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema61000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema61000.ForeColor = System.Drawing.Color.White;
            this.btnTema61000.Location = new System.Drawing.Point(782, 109);
            this.btnTema61000.Name = "btnTema61000";
            this.btnTema61000.Size = new System.Drawing.Size(150, 100);
            this.btnTema61000.TabIndex = 47;
            this.btnTema61000.Text = "1000";
            this.btnTema61000.UseVisualStyleBackColor = false;
            this.btnTema61000.Click += new System.EventHandler(this.btnTema61000_Click);
            // 
            // btnTema51000
            // 
            this.btnTema51000.BackColor = System.Drawing.Color.Blue;
            this.btnTema51000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema51000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema51000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema51000.ForeColor = System.Drawing.Color.White;
            this.btnTema51000.Location = new System.Drawing.Point(626, 109);
            this.btnTema51000.Name = "btnTema51000";
            this.btnTema51000.Size = new System.Drawing.Size(150, 100);
            this.btnTema51000.TabIndex = 46;
            this.btnTema51000.Text = "1000";
            this.btnTema51000.UseVisualStyleBackColor = false;
            this.btnTema51000.Click += new System.EventHandler(this.btnTema51000_Click);
            // 
            // btnTema41000
            // 
            this.btnTema41000.BackColor = System.Drawing.Color.Blue;
            this.btnTema41000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema41000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema41000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema41000.ForeColor = System.Drawing.Color.White;
            this.btnTema41000.Location = new System.Drawing.Point(470, 109);
            this.btnTema41000.Name = "btnTema41000";
            this.btnTema41000.Size = new System.Drawing.Size(150, 100);
            this.btnTema41000.TabIndex = 45;
            this.btnTema41000.Text = "1000";
            this.btnTema41000.UseVisualStyleBackColor = false;
            this.btnTema41000.Click += new System.EventHandler(this.btnTema41000_Click);
            // 
            // btnTema31000
            // 
            this.btnTema31000.BackColor = System.Drawing.Color.Blue;
            this.btnTema31000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema31000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema31000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema31000.ForeColor = System.Drawing.Color.White;
            this.btnTema31000.Location = new System.Drawing.Point(314, 109);
            this.btnTema31000.Name = "btnTema31000";
            this.btnTema31000.Size = new System.Drawing.Size(150, 100);
            this.btnTema31000.TabIndex = 44;
            this.btnTema31000.Text = "1000";
            this.btnTema31000.UseVisualStyleBackColor = false;
            this.btnTema31000.Click += new System.EventHandler(this.btnTema31000_Click);
            // 
            // btnTema21000
            // 
            this.btnTema21000.BackColor = System.Drawing.Color.Blue;
            this.btnTema21000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema21000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema21000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema21000.ForeColor = System.Drawing.Color.White;
            this.btnTema21000.Location = new System.Drawing.Point(158, 109);
            this.btnTema21000.Name = "btnTema21000";
            this.btnTema21000.Size = new System.Drawing.Size(150, 100);
            this.btnTema21000.TabIndex = 43;
            this.btnTema21000.Text = "1000";
            this.btnTema21000.UseVisualStyleBackColor = false;
            this.btnTema21000.Click += new System.EventHandler(this.btnTema21000_Click);
            // 
            // btnTema11000
            // 
            this.btnTema11000.BackColor = System.Drawing.Color.Blue;
            this.btnTema11000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema11000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema11000.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema11000.ForeColor = System.Drawing.Color.White;
            this.btnTema11000.Location = new System.Drawing.Point(2, 109);
            this.btnTema11000.Name = "btnTema11000";
            this.btnTema11000.Size = new System.Drawing.Size(150, 100);
            this.btnTema11000.TabIndex = 42;
            this.btnTema11000.Text = "1000";
            this.btnTema11000.UseVisualStyleBackColor = false;
            this.btnTema11000.Click += new System.EventHandler(this.btnTema11000_Click);
            // 
            // btnTema6Bonus
            // 
            this.btnTema6Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema6Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema6Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema6Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema6Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema6Bonus.Location = new System.Drawing.Point(782, 3);
            this.btnTema6Bonus.Name = "btnTema6Bonus";
            this.btnTema6Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema6Bonus.TabIndex = 41;
            this.btnTema6Bonus.Text = "Téma 6";
            this.btnTema6Bonus.UseVisualStyleBackColor = false;
            this.btnTema6Bonus.Click += new System.EventHandler(this.btnTema6Bonus_Click);
            // 
            // btnTema5Bonus
            // 
            this.btnTema5Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema5Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema5Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema5Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema5Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema5Bonus.Location = new System.Drawing.Point(626, 3);
            this.btnTema5Bonus.Name = "btnTema5Bonus";
            this.btnTema5Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema5Bonus.TabIndex = 40;
            this.btnTema5Bonus.Text = "Téma 5";
            this.btnTema5Bonus.UseVisualStyleBackColor = false;
            this.btnTema5Bonus.Click += new System.EventHandler(this.btnTema5Bonus_Click);
            // 
            // btnTema4Bonus
            // 
            this.btnTema4Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema4Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema4Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema4Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema4Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema4Bonus.Location = new System.Drawing.Point(470, 3);
            this.btnTema4Bonus.Name = "btnTema4Bonus";
            this.btnTema4Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema4Bonus.TabIndex = 39;
            this.btnTema4Bonus.Text = "Téma 4";
            this.btnTema4Bonus.UseVisualStyleBackColor = false;
            this.btnTema4Bonus.Click += new System.EventHandler(this.btnTema4Bonus_Click);
            // 
            // btnTema3Bonus
            // 
            this.btnTema3Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema3Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema3Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema3Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema3Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema3Bonus.Location = new System.Drawing.Point(314, 3);
            this.btnTema3Bonus.Name = "btnTema3Bonus";
            this.btnTema3Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema3Bonus.TabIndex = 38;
            this.btnTema3Bonus.Text = "Téma 3";
            this.btnTema3Bonus.UseVisualStyleBackColor = false;
            this.btnTema3Bonus.Click += new System.EventHandler(this.btnTema3Bonus_Click);
            // 
            // btnTema2Bonus
            // 
            this.btnTema2Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema2Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema2Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema2Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema2Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema2Bonus.Location = new System.Drawing.Point(158, 3);
            this.btnTema2Bonus.Name = "btnTema2Bonus";
            this.btnTema2Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema2Bonus.TabIndex = 37;
            this.btnTema2Bonus.Text = "Téma 2";
            this.btnTema2Bonus.UseVisualStyleBackColor = false;
            this.btnTema2Bonus.Click += new System.EventHandler(this.btnTema2Bonus_Click);
            // 
            // btnTema1Bonus
            // 
            this.btnTema1Bonus.BackColor = System.Drawing.Color.Red;
            this.btnTema1Bonus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTema1Bonus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTema1Bonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnTema1Bonus.ForeColor = System.Drawing.Color.White;
            this.btnTema1Bonus.Location = new System.Drawing.Point(2, 3);
            this.btnTema1Bonus.Name = "btnTema1Bonus";
            this.btnTema1Bonus.Size = new System.Drawing.Size(150, 100);
            this.btnTema1Bonus.TabIndex = 36;
            this.btnTema1Bonus.Text = "Téma 1";
            this.btnTema1Bonus.UseVisualStyleBackColor = false;
            this.btnTema1Bonus.Click += new System.EventHandler(this.btnTema1Bonus_Click);
            // 
            // panelOtazka
            // 
            this.panelOtazka.Controls.Add(this.lOtazka);
            this.panelOtazka.Controls.Add(this.panelCas);
            this.panelOtazka.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOtazka.Location = new System.Drawing.Point(0, 0);
            this.panelOtazka.Name = "panelOtazka";
            this.panelOtazka.Size = new System.Drawing.Size(934, 636);
            this.panelOtazka.TabIndex = 74;
            this.panelOtazka.Visible = false;
            // 
            // lOtazka
            // 
            this.lOtazka.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lOtazka.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lOtazka.ForeColor = System.Drawing.Color.White;
            this.lOtazka.Location = new System.Drawing.Point(0, 42);
            this.lOtazka.Name = "lOtazka";
            this.lOtazka.Size = new System.Drawing.Size(934, 594);
            this.lOtazka.TabIndex = 1;
            this.lOtazka.Text = "Otázka";
            this.lOtazka.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lOtazka.Visible = false;
            this.lOtazka.Click += new System.EventHandler(this.lOtazka_Click);
            // 
            // panelCas
            // 
            this.panelCas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCas.Controls.Add(this.picture12);
            this.panelCas.Controls.Add(this.picture11);
            this.panelCas.Controls.Add(this.picture10);
            this.panelCas.Controls.Add(this.picture9);
            this.panelCas.Controls.Add(this.picture8);
            this.panelCas.Controls.Add(this.picture7);
            this.panelCas.Controls.Add(this.picture6);
            this.panelCas.Controls.Add(this.picture5);
            this.panelCas.Controls.Add(this.picture4);
            this.panelCas.Controls.Add(this.picture3);
            this.panelCas.Controls.Add(this.picture2);
            this.panelCas.Controls.Add(this.picture1);
            this.panelCas.Controls.Add(this.lCasNaOtázku);
            this.panelCas.Controls.Add(this.lPrihlasenyTym);
            this.panelCas.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCas.Location = new System.Drawing.Point(0, 0);
            this.panelCas.Name = "panelCas";
            this.panelCas.Size = new System.Drawing.Size(934, 40);
            this.panelCas.TabIndex = 4;
            // 
            // picture12
            // 
            this.picture12.Image = global::Riskuj.Properties.Resources._02;
            this.picture12.Location = new System.Drawing.Point(799, 10);
            this.picture12.Name = "picture12";
            this.picture12.Size = new System.Drawing.Size(16, 16);
            this.picture12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture12.TabIndex = 18;
            this.picture12.TabStop = false;
            // 
            // picture11
            // 
            this.picture11.Image = global::Riskuj.Properties.Resources._02;
            this.picture11.Location = new System.Drawing.Point(777, 10);
            this.picture11.Name = "picture11";
            this.picture11.Size = new System.Drawing.Size(16, 16);
            this.picture11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture11.TabIndex = 17;
            this.picture11.TabStop = false;
            // 
            // picture10
            // 
            this.picture10.Image = global::Riskuj.Properties.Resources._02;
            this.picture10.Location = new System.Drawing.Point(755, 10);
            this.picture10.Name = "picture10";
            this.picture10.Size = new System.Drawing.Size(16, 16);
            this.picture10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture10.TabIndex = 16;
            this.picture10.TabStop = false;
            // 
            // picture9
            // 
            this.picture9.Image = global::Riskuj.Properties.Resources._02;
            this.picture9.Location = new System.Drawing.Point(733, 10);
            this.picture9.Name = "picture9";
            this.picture9.Size = new System.Drawing.Size(16, 16);
            this.picture9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture9.TabIndex = 15;
            this.picture9.TabStop = false;
            // 
            // picture8
            // 
            this.picture8.Image = global::Riskuj.Properties.Resources._02;
            this.picture8.Location = new System.Drawing.Point(711, 10);
            this.picture8.Name = "picture8";
            this.picture8.Size = new System.Drawing.Size(16, 16);
            this.picture8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture8.TabIndex = 14;
            this.picture8.TabStop = false;
            // 
            // picture7
            // 
            this.picture7.Image = global::Riskuj.Properties.Resources._02;
            this.picture7.Location = new System.Drawing.Point(689, 10);
            this.picture7.Name = "picture7";
            this.picture7.Size = new System.Drawing.Size(16, 16);
            this.picture7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture7.TabIndex = 13;
            this.picture7.TabStop = false;
            // 
            // picture6
            // 
            this.picture6.Image = global::Riskuj.Properties.Resources._02;
            this.picture6.Location = new System.Drawing.Point(667, 10);
            this.picture6.Name = "picture6";
            this.picture6.Size = new System.Drawing.Size(16, 16);
            this.picture6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture6.TabIndex = 12;
            this.picture6.TabStop = false;
            // 
            // picture5
            // 
            this.picture5.Image = global::Riskuj.Properties.Resources._02;
            this.picture5.Location = new System.Drawing.Point(645, 10);
            this.picture5.Name = "picture5";
            this.picture5.Size = new System.Drawing.Size(16, 16);
            this.picture5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture5.TabIndex = 11;
            this.picture5.TabStop = false;
            // 
            // picture4
            // 
            this.picture4.Image = global::Riskuj.Properties.Resources._02;
            this.picture4.Location = new System.Drawing.Point(623, 10);
            this.picture4.Name = "picture4";
            this.picture4.Size = new System.Drawing.Size(16, 16);
            this.picture4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture4.TabIndex = 10;
            this.picture4.TabStop = false;
            // 
            // picture3
            // 
            this.picture3.Image = global::Riskuj.Properties.Resources._02;
            this.picture3.Location = new System.Drawing.Point(601, 10);
            this.picture3.Name = "picture3";
            this.picture3.Size = new System.Drawing.Size(16, 16);
            this.picture3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture3.TabIndex = 9;
            this.picture3.TabStop = false;
            // 
            // picture2
            // 
            this.picture2.Image = global::Riskuj.Properties.Resources._02;
            this.picture2.Location = new System.Drawing.Point(579, 10);
            this.picture2.Name = "picture2";
            this.picture2.Size = new System.Drawing.Size(16, 16);
            this.picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture2.TabIndex = 8;
            this.picture2.TabStop = false;
            // 
            // picture1
            // 
            this.picture1.Image = global::Riskuj.Properties.Resources._02;
            this.picture1.Location = new System.Drawing.Point(557, 10);
            this.picture1.Name = "picture1";
            this.picture1.Size = new System.Drawing.Size(16, 16);
            this.picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picture1.TabIndex = 7;
            this.picture1.TabStop = false;
            // 
            // lCasNaOtázku
            // 
            this.lCasNaOtázku.AutoSize = true;
            this.lCasNaOtázku.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lCasNaOtázku.ForeColor = System.Drawing.Color.Red;
            this.lCasNaOtázku.Location = new System.Drawing.Point(450, 0);
            this.lCasNaOtázku.Name = "lCasNaOtázku";
            this.lCasNaOtázku.Size = new System.Drawing.Size(79, 33);
            this.lCasNaOtázku.TabIndex = 5;
            this.lCasNaOtázku.Text = "Čas:";
            // 
            // lPrihlasenyTym
            // 
            this.lPrihlasenyTym.AutoSize = true;
            this.lPrihlasenyTym.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPrihlasenyTym.ForeColor = System.Drawing.Color.Red;
            this.lPrihlasenyTym.Location = new System.Drawing.Point(10, 0);
            this.lPrihlasenyTym.Name = "lPrihlasenyTym";
            this.lPrihlasenyTym.Size = new System.Drawing.Size(231, 33);
            this.lPrihlasenyTym.TabIndex = 4;
            this.lPrihlasenyTym.Text = "Přihlášený tým:";
            this.lPrihlasenyTym.Click += new System.EventHandler(this.lPrihlasenyTym_Click);
            // 
            // panelKolo2
            // 
            this.panelKolo2.Controls.Add(this.btn2Kolo36);
            this.panelKolo2.Controls.Add(this.btn2Kolo30);
            this.panelKolo2.Controls.Add(this.btn2Kolo24);
            this.panelKolo2.Controls.Add(this.btn2Kolo18);
            this.panelKolo2.Controls.Add(this.btn2Kolo12);
            this.panelKolo2.Controls.Add(this.btn2Kolo6);
            this.panelKolo2.Controls.Add(this.btn2Kolo35);
            this.panelKolo2.Controls.Add(this.btn2Kolo29);
            this.panelKolo2.Controls.Add(this.btn2Kolo23);
            this.panelKolo2.Controls.Add(this.btn2Kolo17);
            this.panelKolo2.Controls.Add(this.btn2Kolo11);
            this.panelKolo2.Controls.Add(this.btn2Kolo5);
            this.panelKolo2.Controls.Add(this.btn2Kolo34);
            this.panelKolo2.Controls.Add(this.btn2Kolo28);
            this.panelKolo2.Controls.Add(this.btn2Kolo22);
            this.panelKolo2.Controls.Add(this.btn2Kolo16);
            this.panelKolo2.Controls.Add(this.btn2Kolo10);
            this.panelKolo2.Controls.Add(this.btn2Kolo4);
            this.panelKolo2.Controls.Add(this.btn2Kolo33);
            this.panelKolo2.Controls.Add(this.btn2Kolo27);
            this.panelKolo2.Controls.Add(this.btn2Kolo21);
            this.panelKolo2.Controls.Add(this.btn2Kolo15);
            this.panelKolo2.Controls.Add(this.btn2Kolo9);
            this.panelKolo2.Controls.Add(this.btn2Kolo3);
            this.panelKolo2.Controls.Add(this.btn2Kolo32);
            this.panelKolo2.Controls.Add(this.btn2Kolo26);
            this.panelKolo2.Controls.Add(this.btn2Kolo20);
            this.panelKolo2.Controls.Add(this.btn2Kolo14);
            this.panelKolo2.Controls.Add(this.btn2Kolo8);
            this.panelKolo2.Controls.Add(this.btn2Kolo2);
            this.panelKolo2.Controls.Add(this.btn2Kolo31);
            this.panelKolo2.Controls.Add(this.btn2Kolo25);
            this.panelKolo2.Controls.Add(this.btn2Kolo19);
            this.panelKolo2.Controls.Add(this.btn2Kolo13);
            this.panelKolo2.Controls.Add(this.btn2Kolo7);
            this.panelKolo2.Controls.Add(this.btn2Kolo1);
            this.panelKolo2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelKolo2.Location = new System.Drawing.Point(0, 0);
            this.panelKolo2.Name = "panelKolo2";
            this.panelKolo2.Size = new System.Drawing.Size(934, 636);
            this.panelKolo2.TabIndex = 75;
            this.panelKolo2.Visible = false;
            // 
            // btn2Kolo36
            // 
            this.btn2Kolo36.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo36.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo36.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo36.Location = new System.Drawing.Point(782, 533);
            this.btn2Kolo36.Name = "btn2Kolo36";
            this.btn2Kolo36.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo36.TabIndex = 78;
            this.btn2Kolo36.Text = "60000";
            this.btn2Kolo36.UseVisualStyleBackColor = false;
            this.btn2Kolo36.Click += new System.EventHandler(this.btn2Kolo36_Click);
            // 
            // btn2Kolo30
            // 
            this.btn2Kolo30.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo30.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo30.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo30.Location = new System.Drawing.Point(626, 533);
            this.btn2Kolo30.Name = "btn2Kolo30";
            this.btn2Kolo30.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo30.TabIndex = 77;
            this.btn2Kolo30.Text = "50000";
            this.btn2Kolo30.UseVisualStyleBackColor = false;
            this.btn2Kolo30.Click += new System.EventHandler(this.btn2Kolo30_Click);
            // 
            // btn2Kolo24
            // 
            this.btn2Kolo24.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo24.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo24.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo24.Location = new System.Drawing.Point(470, 533);
            this.btn2Kolo24.Name = "btn2Kolo24";
            this.btn2Kolo24.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo24.TabIndex = 76;
            this.btn2Kolo24.Text = "40000";
            this.btn2Kolo24.UseVisualStyleBackColor = false;
            this.btn2Kolo24.Click += new System.EventHandler(this.btn2Kolo24_Click);
            // 
            // btn2Kolo18
            // 
            this.btn2Kolo18.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo18.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo18.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo18.Location = new System.Drawing.Point(314, 533);
            this.btn2Kolo18.Name = "btn2Kolo18";
            this.btn2Kolo18.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo18.TabIndex = 75;
            this.btn2Kolo18.Text = "30000";
            this.btn2Kolo18.UseVisualStyleBackColor = false;
            this.btn2Kolo18.Click += new System.EventHandler(this.btn2Kolo18_Click);
            // 
            // btn2Kolo12
            // 
            this.btn2Kolo12.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo12.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo12.Location = new System.Drawing.Point(158, 533);
            this.btn2Kolo12.Name = "btn2Kolo12";
            this.btn2Kolo12.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo12.TabIndex = 74;
            this.btn2Kolo12.Text = "20000";
            this.btn2Kolo12.UseVisualStyleBackColor = false;
            this.btn2Kolo12.Click += new System.EventHandler(this.btn2Kolo12_Click);
            // 
            // btn2Kolo6
            // 
            this.btn2Kolo6.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo6.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo6.Location = new System.Drawing.Point(2, 533);
            this.btn2Kolo6.Name = "btn2Kolo6";
            this.btn2Kolo6.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo6.TabIndex = 73;
            this.btn2Kolo6.Text = "10000";
            this.btn2Kolo6.UseVisualStyleBackColor = false;
            this.btn2Kolo6.Click += new System.EventHandler(this.btn2Kolo6_Click);
            // 
            // btn2Kolo35
            // 
            this.btn2Kolo35.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo35.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo35.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo35.Location = new System.Drawing.Point(781, 427);
            this.btn2Kolo35.Name = "btn2Kolo35";
            this.btn2Kolo35.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo35.TabIndex = 72;
            this.btn2Kolo35.Text = "58000";
            this.btn2Kolo35.UseVisualStyleBackColor = false;
            this.btn2Kolo35.Click += new System.EventHandler(this.btn2Kolo35_Click);
            // 
            // btn2Kolo29
            // 
            this.btn2Kolo29.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo29.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo29.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo29.Location = new System.Drawing.Point(626, 427);
            this.btn2Kolo29.Name = "btn2Kolo29";
            this.btn2Kolo29.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo29.TabIndex = 71;
            this.btn2Kolo29.Text = "48000";
            this.btn2Kolo29.UseVisualStyleBackColor = false;
            this.btn2Kolo29.Click += new System.EventHandler(this.btn2Kolo29_Click);
            // 
            // btn2Kolo23
            // 
            this.btn2Kolo23.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo23.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo23.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo23.Location = new System.Drawing.Point(470, 427);
            this.btn2Kolo23.Name = "btn2Kolo23";
            this.btn2Kolo23.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo23.TabIndex = 70;
            this.btn2Kolo23.Text = "38000";
            this.btn2Kolo23.UseVisualStyleBackColor = false;
            this.btn2Kolo23.Click += new System.EventHandler(this.btn2Kolo23_Click);
            // 
            // btn2Kolo17
            // 
            this.btn2Kolo17.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo17.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo17.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo17.Location = new System.Drawing.Point(314, 427);
            this.btn2Kolo17.Name = "btn2Kolo17";
            this.btn2Kolo17.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo17.TabIndex = 69;
            this.btn2Kolo17.Text = "28000";
            this.btn2Kolo17.UseVisualStyleBackColor = false;
            this.btn2Kolo17.Click += new System.EventHandler(this.btn2Kolo17_Click);
            // 
            // btn2Kolo11
            // 
            this.btn2Kolo11.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo11.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo11.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo11.Location = new System.Drawing.Point(158, 427);
            this.btn2Kolo11.Name = "btn2Kolo11";
            this.btn2Kolo11.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo11.TabIndex = 68;
            this.btn2Kolo11.Text = "18000";
            this.btn2Kolo11.UseVisualStyleBackColor = false;
            this.btn2Kolo11.Click += new System.EventHandler(this.btn2Kolo11_Click);
            // 
            // btn2Kolo5
            // 
            this.btn2Kolo5.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo5.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo5.Location = new System.Drawing.Point(3, 427);
            this.btn2Kolo5.Name = "btn2Kolo5";
            this.btn2Kolo5.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo5.TabIndex = 67;
            this.btn2Kolo5.Text = "8000";
            this.btn2Kolo5.UseVisualStyleBackColor = false;
            this.btn2Kolo5.Click += new System.EventHandler(this.btn2Kolo5_Click);
            // 
            // btn2Kolo34
            // 
            this.btn2Kolo34.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo34.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo34.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo34.Location = new System.Drawing.Point(781, 321);
            this.btn2Kolo34.Name = "btn2Kolo34";
            this.btn2Kolo34.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo34.TabIndex = 66;
            this.btn2Kolo34.Text = "55000";
            this.btn2Kolo34.UseVisualStyleBackColor = false;
            this.btn2Kolo34.Click += new System.EventHandler(this.btn2Kolo34_Click);
            // 
            // btn2Kolo28
            // 
            this.btn2Kolo28.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo28.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo28.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo28.Location = new System.Drawing.Point(626, 321);
            this.btn2Kolo28.Name = "btn2Kolo28";
            this.btn2Kolo28.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo28.TabIndex = 65;
            this.btn2Kolo28.Text = "45000";
            this.btn2Kolo28.UseVisualStyleBackColor = false;
            this.btn2Kolo28.Click += new System.EventHandler(this.btn2Kolo28_Click);
            // 
            // btn2Kolo22
            // 
            this.btn2Kolo22.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo22.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo22.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo22.Location = new System.Drawing.Point(470, 321);
            this.btn2Kolo22.Name = "btn2Kolo22";
            this.btn2Kolo22.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo22.TabIndex = 64;
            this.btn2Kolo22.Text = "35000";
            this.btn2Kolo22.UseVisualStyleBackColor = false;
            this.btn2Kolo22.Click += new System.EventHandler(this.btn2Kolo22_Click);
            // 
            // btn2Kolo16
            // 
            this.btn2Kolo16.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo16.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo16.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo16.Location = new System.Drawing.Point(314, 321);
            this.btn2Kolo16.Name = "btn2Kolo16";
            this.btn2Kolo16.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo16.TabIndex = 63;
            this.btn2Kolo16.Text = "25000";
            this.btn2Kolo16.UseVisualStyleBackColor = false;
            this.btn2Kolo16.Click += new System.EventHandler(this.btn2Kolo16_Click);
            // 
            // btn2Kolo10
            // 
            this.btn2Kolo10.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo10.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo10.Location = new System.Drawing.Point(158, 321);
            this.btn2Kolo10.Name = "btn2Kolo10";
            this.btn2Kolo10.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo10.TabIndex = 62;
            this.btn2Kolo10.Text = "15000";
            this.btn2Kolo10.UseVisualStyleBackColor = false;
            this.btn2Kolo10.Click += new System.EventHandler(this.btn2Kolo10_Click);
            // 
            // btn2Kolo4
            // 
            this.btn2Kolo4.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo4.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo4.Location = new System.Drawing.Point(2, 321);
            this.btn2Kolo4.Name = "btn2Kolo4";
            this.btn2Kolo4.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo4.TabIndex = 61;
            this.btn2Kolo4.Text = "5000";
            this.btn2Kolo4.UseVisualStyleBackColor = false;
            this.btn2Kolo4.Click += new System.EventHandler(this.btn2Kolo4_Click);
            // 
            // btn2Kolo33
            // 
            this.btn2Kolo33.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo33.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo33.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo33.Location = new System.Drawing.Point(781, 215);
            this.btn2Kolo33.Name = "btn2Kolo33";
            this.btn2Kolo33.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo33.TabIndex = 60;
            this.btn2Kolo33.Text = "53000";
            this.btn2Kolo33.UseVisualStyleBackColor = false;
            this.btn2Kolo33.Click += new System.EventHandler(this.btn2Kolo33_Click);
            // 
            // btn2Kolo27
            // 
            this.btn2Kolo27.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo27.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo27.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo27.Location = new System.Drawing.Point(626, 215);
            this.btn2Kolo27.Name = "btn2Kolo27";
            this.btn2Kolo27.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo27.TabIndex = 59;
            this.btn2Kolo27.Text = "43000";
            this.btn2Kolo27.UseVisualStyleBackColor = false;
            this.btn2Kolo27.Click += new System.EventHandler(this.btn2Kolo27_Click);
            // 
            // btn2Kolo21
            // 
            this.btn2Kolo21.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo21.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo21.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo21.Location = new System.Drawing.Point(470, 215);
            this.btn2Kolo21.Name = "btn2Kolo21";
            this.btn2Kolo21.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo21.TabIndex = 58;
            this.btn2Kolo21.Text = "33000";
            this.btn2Kolo21.UseVisualStyleBackColor = false;
            this.btn2Kolo21.Click += new System.EventHandler(this.btn2Kolo21_Click);
            // 
            // btn2Kolo15
            // 
            this.btn2Kolo15.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo15.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo15.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo15.Location = new System.Drawing.Point(314, 215);
            this.btn2Kolo15.Name = "btn2Kolo15";
            this.btn2Kolo15.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo15.TabIndex = 57;
            this.btn2Kolo15.Text = "23000";
            this.btn2Kolo15.UseVisualStyleBackColor = false;
            this.btn2Kolo15.Click += new System.EventHandler(this.btn2Kolo15_Click);
            // 
            // btn2Kolo9
            // 
            this.btn2Kolo9.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo9.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo9.Location = new System.Drawing.Point(158, 215);
            this.btn2Kolo9.Name = "btn2Kolo9";
            this.btn2Kolo9.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo9.TabIndex = 56;
            this.btn2Kolo9.Text = "13000";
            this.btn2Kolo9.UseVisualStyleBackColor = false;
            this.btn2Kolo9.Click += new System.EventHandler(this.btn2Kolo9_Click);
            // 
            // btn2Kolo3
            // 
            this.btn2Kolo3.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo3.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo3.Location = new System.Drawing.Point(2, 215);
            this.btn2Kolo3.Name = "btn2Kolo3";
            this.btn2Kolo3.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo3.TabIndex = 55;
            this.btn2Kolo3.Text = "3000";
            this.btn2Kolo3.UseVisualStyleBackColor = false;
            this.btn2Kolo3.Click += new System.EventHandler(this.btn2Kolo3_Click);
            // 
            // btn2Kolo32
            // 
            this.btn2Kolo32.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo32.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo32.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo32.Location = new System.Drawing.Point(781, 109);
            this.btn2Kolo32.Name = "btn2Kolo32";
            this.btn2Kolo32.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo32.TabIndex = 54;
            this.btn2Kolo32.Text = "52000";
            this.btn2Kolo32.UseVisualStyleBackColor = false;
            this.btn2Kolo32.Click += new System.EventHandler(this.btn2Kolo32_Click);
            // 
            // btn2Kolo26
            // 
            this.btn2Kolo26.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo26.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo26.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo26.Location = new System.Drawing.Point(626, 109);
            this.btn2Kolo26.Name = "btn2Kolo26";
            this.btn2Kolo26.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo26.TabIndex = 53;
            this.btn2Kolo26.Text = "42000";
            this.btn2Kolo26.UseVisualStyleBackColor = false;
            this.btn2Kolo26.Click += new System.EventHandler(this.btn2Kolo26_Click);
            // 
            // btn2Kolo20
            // 
            this.btn2Kolo20.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo20.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo20.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo20.Location = new System.Drawing.Point(470, 109);
            this.btn2Kolo20.Name = "btn2Kolo20";
            this.btn2Kolo20.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo20.TabIndex = 52;
            this.btn2Kolo20.Text = "32000";
            this.btn2Kolo20.UseVisualStyleBackColor = false;
            this.btn2Kolo20.Click += new System.EventHandler(this.btn2Kolo20_Click);
            // 
            // btn2Kolo14
            // 
            this.btn2Kolo14.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo14.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo14.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo14.Location = new System.Drawing.Point(314, 109);
            this.btn2Kolo14.Name = "btn2Kolo14";
            this.btn2Kolo14.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo14.TabIndex = 51;
            this.btn2Kolo14.Text = "22000";
            this.btn2Kolo14.UseVisualStyleBackColor = false;
            this.btn2Kolo14.Click += new System.EventHandler(this.btn2Kolo14_Click);
            // 
            // btn2Kolo8
            // 
            this.btn2Kolo8.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo8.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo8.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo8.Location = new System.Drawing.Point(158, 109);
            this.btn2Kolo8.Name = "btn2Kolo8";
            this.btn2Kolo8.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo8.TabIndex = 50;
            this.btn2Kolo8.Text = "12000";
            this.btn2Kolo8.UseVisualStyleBackColor = false;
            this.btn2Kolo8.Click += new System.EventHandler(this.btn2Kolo8_Click);
            // 
            // btn2Kolo2
            // 
            this.btn2Kolo2.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo2.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo2.Location = new System.Drawing.Point(2, 109);
            this.btn2Kolo2.Name = "btn2Kolo2";
            this.btn2Kolo2.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo2.TabIndex = 49;
            this.btn2Kolo2.Text = "2000";
            this.btn2Kolo2.UseVisualStyleBackColor = false;
            this.btn2Kolo2.Click += new System.EventHandler(this.btn2Kolo2_Click);
            // 
            // btn2Kolo31
            // 
            this.btn2Kolo31.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo31.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo31.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo31.Location = new System.Drawing.Point(781, 3);
            this.btn2Kolo31.Name = "btn2Kolo31";
            this.btn2Kolo31.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo31.TabIndex = 48;
            this.btn2Kolo31.Text = "51000";
            this.btn2Kolo31.UseVisualStyleBackColor = false;
            this.btn2Kolo31.Click += new System.EventHandler(this.btn2Kolo31_Click);
            // 
            // btn2Kolo25
            // 
            this.btn2Kolo25.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo25.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo25.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo25.Location = new System.Drawing.Point(626, 3);
            this.btn2Kolo25.Name = "btn2Kolo25";
            this.btn2Kolo25.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo25.TabIndex = 47;
            this.btn2Kolo25.Text = "41000";
            this.btn2Kolo25.UseVisualStyleBackColor = false;
            this.btn2Kolo25.Click += new System.EventHandler(this.btn2Kolo25_Click);
            // 
            // btn2Kolo19
            // 
            this.btn2Kolo19.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo19.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo19.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo19.Location = new System.Drawing.Point(470, 3);
            this.btn2Kolo19.Name = "btn2Kolo19";
            this.btn2Kolo19.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo19.TabIndex = 46;
            this.btn2Kolo19.Text = "31000";
            this.btn2Kolo19.UseVisualStyleBackColor = false;
            this.btn2Kolo19.Click += new System.EventHandler(this.btn2Kolo19_Click);
            // 
            // btn2Kolo13
            // 
            this.btn2Kolo13.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo13.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo13.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo13.Location = new System.Drawing.Point(314, 3);
            this.btn2Kolo13.Name = "btn2Kolo13";
            this.btn2Kolo13.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo13.TabIndex = 45;
            this.btn2Kolo13.Text = "21000";
            this.btn2Kolo13.UseVisualStyleBackColor = false;
            this.btn2Kolo13.Click += new System.EventHandler(this.btn2Kolo13_Click);
            // 
            // btn2Kolo7
            // 
            this.btn2Kolo7.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo7.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo7.Location = new System.Drawing.Point(158, 3);
            this.btn2Kolo7.Name = "btn2Kolo7";
            this.btn2Kolo7.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo7.TabIndex = 44;
            this.btn2Kolo7.Text = "11000";
            this.btn2Kolo7.UseVisualStyleBackColor = false;
            this.btn2Kolo7.Click += new System.EventHandler(this.btn2Kolo7_Click);
            // 
            // btn2Kolo1
            // 
            this.btn2Kolo1.BackColor = System.Drawing.Color.Blue;
            this.btn2Kolo1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2Kolo1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn2Kolo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn2Kolo1.ForeColor = System.Drawing.Color.White;
            this.btn2Kolo1.Location = new System.Drawing.Point(2, 3);
            this.btn2Kolo1.Name = "btn2Kolo1";
            this.btn2Kolo1.Size = new System.Drawing.Size(150, 100);
            this.btn2Kolo1.TabIndex = 43;
            this.btn2Kolo1.Text = "1000";
            this.btn2Kolo1.UseVisualStyleBackColor = false;
            this.btn2Kolo1.Click += new System.EventHandler(this.btn2Kolo1_Click);
            // 
            // panelKonec
            // 
            this.panelKonec.Controls.Add(this.lKonec);
            this.panelKonec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelKonec.Location = new System.Drawing.Point(0, 0);
            this.panelKonec.Name = "panelKonec";
            this.panelKonec.Size = new System.Drawing.Size(934, 636);
            this.panelKonec.TabIndex = 76;
            this.panelKonec.Visible = false;
            // 
            // lKonec
            // 
            this.lKonec.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lKonec.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lKonec.ForeColor = System.Drawing.Color.White;
            this.lKonec.Location = new System.Drawing.Point(0, 0);
            this.lKonec.Name = "lKonec";
            this.lKonec.Size = new System.Drawing.Size(934, 636);
            this.lKonec.TabIndex = 1;
            this.lKonec.Text = "KONEC HRY";
            this.lKonec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lKonec.Visible = false;
            // 
            // panelKolo3
            // 
            this.panelKolo3.Controls.Add(this.btnRubl);
            this.panelKolo3.Controls.Add(this.btnRupie);
            this.panelKolo3.Controls.Add(this.btnEuro);
            this.panelKolo3.Controls.Add(this.btnLibra);
            this.panelKolo3.Controls.Add(this.btnDolar);
            this.panelKolo3.Controls.Add(this.btnKoruna);
            this.panelKolo3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelKolo3.Location = new System.Drawing.Point(0, 0);
            this.panelKolo3.Name = "panelKolo3";
            this.panelKolo3.Size = new System.Drawing.Size(934, 636);
            this.panelKolo3.TabIndex = 77;
            this.panelKolo3.Visible = false;
            // 
            // btnRubl
            // 
            this.btnRubl.BackColor = System.Drawing.Color.Blue;
            this.btnRubl.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRubl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRubl.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnRubl.ForeColor = System.Drawing.Color.White;
            this.btnRubl.Location = new System.Drawing.Point(531, 299);
            this.btnRubl.Name = "btnRubl";
            this.btnRubl.Size = new System.Drawing.Size(150, 100);
            this.btnRubl.TabIndex = 49;
            this.btnRubl.Text = "Rubl";
            this.btnRubl.UseVisualStyleBackColor = false;
            this.btnRubl.Click += new System.EventHandler(this.btnRubl_Click);
            // 
            // btnRupie
            // 
            this.btnRupie.BackColor = System.Drawing.Color.Blue;
            this.btnRupie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRupie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRupie.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnRupie.ForeColor = System.Drawing.Color.White;
            this.btnRupie.Location = new System.Drawing.Point(375, 299);
            this.btnRupie.Name = "btnRupie";
            this.btnRupie.Size = new System.Drawing.Size(150, 100);
            this.btnRupie.TabIndex = 48;
            this.btnRupie.Text = "Rupie";
            this.btnRupie.UseVisualStyleBackColor = false;
            this.btnRupie.Click += new System.EventHandler(this.btnRupie_Click);
            // 
            // btnEuro
            // 
            this.btnEuro.BackColor = System.Drawing.Color.Blue;
            this.btnEuro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEuro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEuro.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnEuro.ForeColor = System.Drawing.Color.White;
            this.btnEuro.Location = new System.Drawing.Point(219, 299);
            this.btnEuro.Name = "btnEuro";
            this.btnEuro.Size = new System.Drawing.Size(150, 100);
            this.btnEuro.TabIndex = 47;
            this.btnEuro.Text = "Euro";
            this.btnEuro.UseVisualStyleBackColor = false;
            this.btnEuro.Click += new System.EventHandler(this.btnEuro_Click);
            // 
            // btnLibra
            // 
            this.btnLibra.BackColor = System.Drawing.Color.Blue;
            this.btnLibra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLibra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLibra.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLibra.ForeColor = System.Drawing.Color.White;
            this.btnLibra.Location = new System.Drawing.Point(531, 193);
            this.btnLibra.Name = "btnLibra";
            this.btnLibra.Size = new System.Drawing.Size(150, 100);
            this.btnLibra.TabIndex = 46;
            this.btnLibra.Text = "Libra";
            this.btnLibra.UseVisualStyleBackColor = false;
            this.btnLibra.Click += new System.EventHandler(this.btnLibra_Click);
            // 
            // btnDolar
            // 
            this.btnDolar.BackColor = System.Drawing.Color.Blue;
            this.btnDolar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDolar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDolar.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnDolar.ForeColor = System.Drawing.Color.White;
            this.btnDolar.Location = new System.Drawing.Point(375, 193);
            this.btnDolar.Name = "btnDolar";
            this.btnDolar.Size = new System.Drawing.Size(150, 100);
            this.btnDolar.TabIndex = 45;
            this.btnDolar.Text = "Dolar";
            this.btnDolar.UseVisualStyleBackColor = false;
            this.btnDolar.Click += new System.EventHandler(this.btnDolar_Click);
            // 
            // btnKoruna
            // 
            this.btnKoruna.BackColor = System.Drawing.Color.Blue;
            this.btnKoruna.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnKoruna.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKoruna.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnKoruna.ForeColor = System.Drawing.Color.White;
            this.btnKoruna.Location = new System.Drawing.Point(219, 193);
            this.btnKoruna.Name = "btnKoruna";
            this.btnKoruna.Size = new System.Drawing.Size(150, 100);
            this.btnKoruna.TabIndex = 44;
            this.btnKoruna.Text = "Koruna";
            this.btnKoruna.UseVisualStyleBackColor = false;
            this.btnKoruna.Click += new System.EventHandler(this.btnKoruna_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Riskuj soubory (*.ris)|*.ris";
            this.openFileDialog.Title = "Načti soubor s otázkami";
            // 
            // timerOdpoved
            // 
            this.timerOdpoved.Interval = 1000;
            this.timerOdpoved.Tick += new System.EventHandler(this.timerOdpoved_Tick);
            // 
            // frmHlavni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(934, 636);
            this.Controls.Add(this.panelKonec);
            this.Controls.Add(this.panelKolo2);
            this.Controls.Add(this.panelKolo1);
            this.Controls.Add(this.panelKolo3);
            this.Controls.Add(this.panelOtazka);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmHlavni";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Riskuj";
            this.Activated += new System.EventHandler(this.frmHlavni_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHlavni_FormClosing);
            this.Load += new System.EventHandler(this.frmHlavni_Load);
            this.Shown += new System.EventHandler(this.frmHlavni_Shown);
            this.panelKolo1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnTema65000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema55000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema45000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema35000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema25000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema15000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema64000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema54000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema44000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema34000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema24000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema14000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema63000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema53000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema43000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema33000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema23000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema13000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema62000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema52000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema42000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema32000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema22000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema12000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema61000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema51000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema41000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema31000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema21000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema11000)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema6Bonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema5Bonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema4Bonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema3Bonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema2Bonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTema1Bonus)).EndInit();
            this.panelOtazka.ResumeLayout(false);
            this.panelCas.ResumeLayout(false);
            this.panelCas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            this.panelKolo2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn2Kolo1)).EndInit();
            this.panelKonec.ResumeLayout(false);
            this.panelKolo3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnRubl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRupie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEuro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLibra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDolar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKoruna)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelKolo1;
        private C1.Win.C1Input.C1Button btnTema65000;
        private C1.Win.C1Input.C1Button btnTema55000;
        private C1.Win.C1Input.C1Button btnTema45000;
        private C1.Win.C1Input.C1Button btnTema35000;
        private C1.Win.C1Input.C1Button btnTema25000;
        private C1.Win.C1Input.C1Button btnTema15000;
        private C1.Win.C1Input.C1Button btnTema64000;
        private C1.Win.C1Input.C1Button btnTema54000;
        private C1.Win.C1Input.C1Button btnTema44000;
        private C1.Win.C1Input.C1Button btnTema34000;
        private C1.Win.C1Input.C1Button btnTema24000;
        private C1.Win.C1Input.C1Button btnTema14000;
        private C1.Win.C1Input.C1Button btnTema63000;
        private C1.Win.C1Input.C1Button btnTema53000;
        private C1.Win.C1Input.C1Button btnTema43000;
        private C1.Win.C1Input.C1Button btnTema33000;
        private C1.Win.C1Input.C1Button btnTema23000;
        private C1.Win.C1Input.C1Button btnTema13000;
        private C1.Win.C1Input.C1Button btnTema62000;
        private C1.Win.C1Input.C1Button btnTema52000;
        private C1.Win.C1Input.C1Button btnTema42000;
        private C1.Win.C1Input.C1Button btnTema32000;
        private C1.Win.C1Input.C1Button btnTema22000;
        private C1.Win.C1Input.C1Button btnTema12000;
        private C1.Win.C1Input.C1Button btnTema61000;
        private C1.Win.C1Input.C1Button btnTema51000;
        private C1.Win.C1Input.C1Button btnTema41000;
        private C1.Win.C1Input.C1Button btnTema31000;
        private C1.Win.C1Input.C1Button btnTema21000;
        private C1.Win.C1Input.C1Button btnTema11000;
        private C1.Win.C1Input.C1Button btnTema6Bonus;
        private C1.Win.C1Input.C1Button btnTema5Bonus;
        private C1.Win.C1Input.C1Button btnTema4Bonus;
        private C1.Win.C1Input.C1Button btnTema3Bonus;
        private C1.Win.C1Input.C1Button btnTema2Bonus;
        private C1.Win.C1Input.C1Button btnTema1Bonus;
        private System.Windows.Forms.Panel panelOtazka;
        private System.Windows.Forms.Label lOtazka;
        private System.Windows.Forms.Panel panelKolo2;
        private C1.Win.C1Input.C1Button btn2Kolo36;
        private C1.Win.C1Input.C1Button btn2Kolo30;
        private C1.Win.C1Input.C1Button btn2Kolo24;
        private C1.Win.C1Input.C1Button btn2Kolo18;
        private C1.Win.C1Input.C1Button btn2Kolo12;
        private C1.Win.C1Input.C1Button btn2Kolo6;
        private C1.Win.C1Input.C1Button btn2Kolo35;
        private C1.Win.C1Input.C1Button btn2Kolo29;
        private C1.Win.C1Input.C1Button btn2Kolo23;
        private C1.Win.C1Input.C1Button btn2Kolo17;
        private C1.Win.C1Input.C1Button btn2Kolo11;
        private C1.Win.C1Input.C1Button btn2Kolo5;
        private C1.Win.C1Input.C1Button btn2Kolo34;
        private C1.Win.C1Input.C1Button btn2Kolo28;
        private C1.Win.C1Input.C1Button btn2Kolo22;
        private C1.Win.C1Input.C1Button btn2Kolo16;
        private C1.Win.C1Input.C1Button btn2Kolo10;
        private C1.Win.C1Input.C1Button btn2Kolo4;
        private C1.Win.C1Input.C1Button btn2Kolo33;
        private C1.Win.C1Input.C1Button btn2Kolo27;
        private C1.Win.C1Input.C1Button btn2Kolo21;
        private C1.Win.C1Input.C1Button btn2Kolo15;
        private C1.Win.C1Input.C1Button btn2Kolo9;
        private C1.Win.C1Input.C1Button btn2Kolo3;
        private C1.Win.C1Input.C1Button btn2Kolo32;
        private C1.Win.C1Input.C1Button btn2Kolo26;
        private C1.Win.C1Input.C1Button btn2Kolo20;
        private C1.Win.C1Input.C1Button btn2Kolo14;
        private C1.Win.C1Input.C1Button btn2Kolo8;
        private C1.Win.C1Input.C1Button btn2Kolo2;
        private C1.Win.C1Input.C1Button btn2Kolo31;
        private C1.Win.C1Input.C1Button btn2Kolo25;
        private C1.Win.C1Input.C1Button btn2Kolo19;
        private C1.Win.C1Input.C1Button btn2Kolo13;
        private C1.Win.C1Input.C1Button btn2Kolo7;
        private C1.Win.C1Input.C1Button btn2Kolo1;
        private System.Windows.Forms.Panel panelKonec;
        private System.Windows.Forms.Label lKonec;
        private System.Windows.Forms.Panel panelKolo3;
        private C1.Win.C1Input.C1Button btnRubl;
        private C1.Win.C1Input.C1Button btnRupie;
        private C1.Win.C1Input.C1Button btnEuro;
        private C1.Win.C1Input.C1Button btnLibra;
        private C1.Win.C1Input.C1Button btnDolar;
        private C1.Win.C1Input.C1Button btnKoruna;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel panelCas;
        private System.Windows.Forms.Label lPrihlasenyTym;
        private System.Windows.Forms.Label lCasNaOtázku;
        private System.Windows.Forms.PictureBox picture10;
        private System.Windows.Forms.PictureBox picture9;
        private System.Windows.Forms.PictureBox picture8;
        private System.Windows.Forms.PictureBox picture7;
        private System.Windows.Forms.PictureBox picture6;
        private System.Windows.Forms.PictureBox picture5;
        private System.Windows.Forms.PictureBox picture4;
        private System.Windows.Forms.PictureBox picture3;
        private System.Windows.Forms.PictureBox picture2;
        private System.Windows.Forms.PictureBox picture1;
        private System.Windows.Forms.PictureBox picture12;
        private System.Windows.Forms.PictureBox picture11;
        private System.Windows.Forms.Timer timerOdpoved;
    }
}

