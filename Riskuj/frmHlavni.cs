﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Media;

namespace Riskuj
{
    public partial class frmHlavni : Form
    {
        private RiskujButton.RiskujButton m_sp;
        public frmHlavni()
        {
            InitializeComponent();
            m_sp = new RiskujButton.RiskujButton(this);
            m_sp.ButtonPressed += sp_ButtonPressed;
        }

        private frmVysledky formVysledky = new frmVysledky();
        Stopwatch stopky = new Stopwatch();

        bool nezachytavat = true;
        int casOdpovedi = 0;
        List<int> stisknuto = new List<int>();
        List<int> zakazano = new List<int>();
        

        List<string> otazky1Kolo = new List<string>();
        List<string> odpovedi1Kolo = new List<string>();
        List<string> otazky2Kolo = new List<string>();
        List<string> odpovedi2Kolo = new List<string>();
        List<string> otazky3Kolo = new List<string>();
        List<string> odpovedi3Kolo = new List<string>();

        private int CasDruheKolo = 1;
        private int pocetOtazek1Kolo = 0;
        private int pocetOtazek2Kolo = 0;
        private int pocetOtazek3Kolo = 0;
        private int Castka = 0;
        private bool JizSpusteneDruheKolo = false;
        private bool JizSpusteneTretiKolo = false;
        private bool OtazkaNeboOdpoved = true;
        private int aktualniKolo = 1;
        private int aktualniOtazka = 0;

        private void sp_ButtonPressed(object sender, int buttonNo)
        {
            stisknuto.Add(buttonNo);

            if (!nezachytavat)
            {
                if (buttonNo > 0 && buttonNo < 4)
                {
                    for (int i = 0; i < stisknuto.Count; i++)
                    {
                        if (stisknuto[i] != 0)
                        {
                            if (!zakazano.Contains(stisknuto[i]))
                            {
                                lPrihlasenyTym.Text = "Přihlášený tým: " + stisknuto[i];

                                zakazano.Add(stisknuto[i]);
                                nezachytavat = true;

                                timerOdpoved.Enabled = true;
                                casOdpovedi = 0;
                            }
                        }
                    }
                }
                else lPrihlasenyTym.Text = "Přihlášený tým: ";
            }           
        }

        private void prvniKoloRadek1(int y)
        {
            switch (y)
            {
                case 0: btnTema1Bonus.Enabled = false; ;
                    break;
                case 1: btnTema11000.Enabled = false; ;
                    break;
                case 2: btnTema12000.Enabled = false; ;
                    break;
                case 3: btnTema13000.Enabled = false; ;
                    break;
                case 4: btnTema14000.Enabled = false; ;
                    break;
                case 5: btnTema15000.Enabled = false; ;
                    break;
            }
        }

        private void prvniKoloRadek2(int y)
        {
            switch (y)
            {
                case 0: btnTema2Bonus.Enabled = false; ;
                    break;
                case 1: btnTema21000.Enabled = false; ;
                    break;
                case 2: btnTema22000.Enabled = false; ;
                    break;
                case 3: btnTema23000.Enabled = false; ;
                    break;
                case 4: btnTema24000.Enabled = false; ;
                    break;
                case 5: btnTema25000.Enabled = false; ;
                    break;
            }
        }

        private void prvniKoloRadek3(int y)
        {
            switch (y)
            {
                case 0: btnTema3Bonus.Enabled = false; ;
                    break;
                case 1: btnTema31000.Enabled = false; ;
                    break;
                case 2: btnTema32000.Enabled = false; ;
                    break;
                case 3: btnTema33000.Enabled = false; ;
                    break;
                case 4: btnTema34000.Enabled = false; ;
                    break;
                case 5: btnTema35000.Enabled = false; ;
                    break;
            }
        }

        private void prvniKoloRadek4(int y)
        {
            switch (y)
            {
                case 0: btnTema4Bonus.Enabled = false; ;
                    break;
                case 1: btnTema41000.Enabled = false; ;
                    break;
                case 2: btnTema42000.Enabled = false; ;
                    break;
                case 3: btnTema43000.Enabled = false; ;
                    break;
                case 4: btnTema44000.Enabled = false; ;
                    break;
                case 5: btnTema45000.Enabled = false; ;
                    break;
            }
        }

        private void prvniKoloRadek5(int y)
        {
            switch (y)
            {
                case 0: btnTema5Bonus.Enabled = false; ;
                    break;
                case 1: btnTema51000.Enabled = false; ;
                    break;
                case 2: btnTema52000.Enabled = false; ;
                    break;
                case 3: btnTema53000.Enabled = false; ;
                    break;
                case 4: btnTema54000.Enabled = false; ;
                    break;
                case 5: btnTema55000.Enabled = false; ;
                    break;
            }
        }

        private void prvniKoloRadek6(int y)
        {
            switch (y)
            {
                case 0: btnTema6Bonus.Enabled = false; ;
                    break;
                case 1: btnTema61000.Enabled = false; ;
                    break;
                case 2: btnTema62000.Enabled = false; ;
                    break;
                case 3: btnTema63000.Enabled = false; ;
                    break;
                case 4: btnTema64000.Enabled = false; ;
                    break;
                case 5: btnTema65000.Enabled = false; ;
                    break;
            }
        }

        private string priradOtazku(int x, int y, int kolo)
        {
            nezachytavat = false;
            casOdpovedi = 0;
            stisknuto.Clear();
            zakazano.Clear();
            reloadPicture();

            string otazka = "";

            if (OtazkaNeboOdpoved)
            {
                if (kolo == 1)
                {
                    if (y > 0) panelCas.Visible = true;
                    else panelCas.Visible = false;

                    aktualniKolo = 1;

                    if (x == 0 && y == 0)
                    {
                        otazka = otazky1Kolo[0];
                        aktualniOtazka = 0;
                    }

                    if (x == 0 && y == 1)
                    {
                        otazka = otazky1Kolo[1];
                        aktualniOtazka = 1;
                    }

                    if (x == 0 && y == 2)
                    {
                        otazka = otazky1Kolo[2];
                        aktualniOtazka = 2;
                    }

                    if (x == 0 && y == 3)
                    {
                        otazka = otazky1Kolo[3];
                        aktualniOtazka = 3;
                    }

                    if (x == 0 && y == 4)
                    {
                        otazka = otazky1Kolo[4];
                        aktualniOtazka = 4;
                    }

                    if (x == 0 && y == 5)
                    {
                        otazka = otazky1Kolo[5];
                        aktualniOtazka = 5;
                    }

                    if (x == 1 && y == 0)
                    {
                        otazka = otazky1Kolo[6];
                        aktualniOtazka = 6;
                    }

                    if (x == 1 && y == 1)
                    {
                        otazka = otazky1Kolo[7];
                        aktualniOtazka = 7;
                    }

                    if (x == 1 && y == 2)
                    {
                        otazka = otazky1Kolo[8];
                        aktualniOtazka = 8;
                    }

                    if (x == 1 && y == 3)
                    {
                        otazka = otazky1Kolo[9];
                        aktualniOtazka = 9;
                    }

                    if (x == 1 && y == 4)
                    {
                        otazka = otazky1Kolo[10];
                        aktualniOtazka = 10;
                    }

                    if (x == 1 && y == 5)
                    {
                        otazka = otazky1Kolo[11];
                        aktualniOtazka = 11;
                    }

                    if (x == 2 && y == 0)
                    {
                        otazka = otazky1Kolo[12];
                        aktualniOtazka = 12;
                    }

                    if (x == 2 && y == 1)
                    {
                        otazka = otazky1Kolo[13];
                        aktualniOtazka = 13;
                    }

                    if (x == 2 && y == 2)
                    {
                        otazka = otazky1Kolo[14];
                        aktualniOtazka = 14;
                    }

                    if (x == 2 && y == 3)
                    {
                        otazka = otazky1Kolo[15];
                        aktualniOtazka = 15;
                    }

                    if (x == 2 && y == 4)
                    {
                        otazka = otazky1Kolo[16];
                        aktualniOtazka = 16;
                    }

                    if (x == 2 && y == 5)
                    {
                        otazka = otazky1Kolo[17];
                        aktualniOtazka = 17;
                    }

                    if (x == 3 && y == 0)
                    {
                        otazka = otazky1Kolo[18];
                        aktualniOtazka = 18;
                    }

                    if (x == 3 && y == 1)
                    {
                        otazka = otazky1Kolo[19];
                        aktualniOtazka = 19;
                    }

                    if (x == 3 && y == 2)
                    {
                        otazka = otazky1Kolo[20];
                        aktualniOtazka = 20;
                    }

                    if (x == 3 && y == 3)
                    {
                        otazka = otazky1Kolo[21];
                        aktualniOtazka = 21;
                    }

                    if (x == 3 && y == 4)
                    {
                        otazka = otazky1Kolo[22];
                        aktualniOtazka = 22;
                    }

                    if (x == 3 && y == 5)
                    {
                        otazka = otazky1Kolo[23];
                        aktualniOtazka = 23;
                    }

                    if (x == 4 && y == 0)
                    {
                        otazka = otazky1Kolo[24];
                        aktualniOtazka = 24;
                    }

                    if (x == 4 && y == 1)
                    {
                        otazka = otazky1Kolo[25];
                        aktualniOtazka = 25;
                    }

                    if (x == 4 && y == 2)
                    {
                        otazka = otazky1Kolo[26];
                        aktualniOtazka = 26;
                    }

                    if (x == 4 && y == 3)
                    {
                        otazka = otazky1Kolo[27];
                        aktualniOtazka = 27;
                    }

                    if (x == 4 && y == 4)
                    {
                        otazka = otazky1Kolo[28];
                        aktualniOtazka = 28;
                    }

                    if (x == 4 && y == 5)
                    {
                        otazka = otazky1Kolo[29];
                        aktualniOtazka = 29;
                    }

                    if (x == 5 && y == 0)
                    {
                        otazka = otazky1Kolo[30];
                        aktualniOtazka = 30;
                    }

                    if (x == 5 && y == 1)
                    {
                        otazka = otazky1Kolo[31];
                        aktualniOtazka = 31;
                    }

                    if (x == 5 && y == 2)
                    {
                        otazka = otazky1Kolo[32];
                        aktualniOtazka = 32;
                    }

                    if (x == 5 && y == 3)
                    {
                        otazka = otazky1Kolo[33];
                        aktualniOtazka = 33;
                    }

                    if (x == 5 && y == 4)
                    {
                        otazka = otazky1Kolo[34];
                        aktualniOtazka = 34;
                    }

                    if (x == 5 && y == 5)
                    {
                        otazka = otazky1Kolo[35];
                        aktualniOtazka = 35;
                    }
                }

                if (kolo == 2)
                {
                    panelCas.Visible = false;

                    aktualniKolo = 2;

                    if (x == 0)
                    {
                        otazka = otazky2Kolo[0];
                        aktualniOtazka = 0;
                    }

                    if (x == 1)
                    {
                        otazka = otazky2Kolo[1];
                        aktualniOtazka = 1;
                    }

                    if (x == 2)
                    {
                        otazka = otazky2Kolo[2];
                        aktualniOtazka = 2;
                    }

                    if (x == 3)
                    {
                        otazka = otazky2Kolo[3];
                        aktualniOtazka = 3;
                    }

                    if (x == 4)
                    {
                        otazka = otazky2Kolo[4];
                        aktualniOtazka = 4;
                    }

                    if (x == 5)
                    {
                        otazka = otazky2Kolo[5];
                        aktualniOtazka = 5;
                    }

                    if (x == 6)
                    {
                        otazka = otazky2Kolo[6];
                        aktualniOtazka = 6;
                    }

                    if (x == 7)
                    {
                        otazka = otazky2Kolo[7];
                        aktualniOtazka = 7;
                    }

                    if (x == 8)
                    {
                        otazka = otazky2Kolo[8];
                        aktualniOtazka = 8;
                    }

                    if (x == 9)
                    {
                        otazka = otazky2Kolo[9];
                        aktualniOtazka = 9;
                    }

                    if (x == 10)
                    {
                        otazka = otazky2Kolo[10];
                        aktualniOtazka = 10;
                    }

                    if (x == 11)
                    {
                        otazka = otazky2Kolo[11];
                        aktualniOtazka = 11;
                    }

                    if (x == 12)
                    {
                        otazka = otazky2Kolo[12];
                        aktualniOtazka = 12;
                    }

                    if (x == 13)
                    {
                        otazka = otazky2Kolo[13];
                        aktualniOtazka = 13;
                    }

                    if (x == 14)
                    {
                        otazka = otazky2Kolo[14];
                        aktualniOtazka = 14;
                    }

                    if (x == 15)
                    {
                        otazka = otazky2Kolo[15];
                        aktualniOtazka = 15;
                    }

                    if (x == 16)
                    {
                        otazka = otazky2Kolo[16];
                        aktualniOtazka = 16;
                    }

                    if (x == 17)
                    {
                        otazka = otazky2Kolo[17];
                        aktualniOtazka = 17;
                    }

                    if (x == 18)
                    {
                        otazka = otazky2Kolo[18];
                        aktualniOtazka = 18;
                    }

                    if (x == 19)
                    {
                        otazka = otazky2Kolo[19];
                        aktualniOtazka = 19;
                    }

                    if (x == 20)
                    {
                        otazka = otazky2Kolo[20];
                        aktualniOtazka = 20;
                    }

                    if (x == 21)
                    {
                        otazka = otazky2Kolo[21];
                        aktualniOtazka = 21;
                    }

                    if (x == 22)
                    {
                        otazka = otazky2Kolo[22];
                        aktualniOtazka = 22;
                    }

                    if (x == 23)
                    {
                        otazka = otazky2Kolo[23];
                        aktualniOtazka = 23;
                    }

                    if (x == 24)
                    {
                        otazka = otazky2Kolo[24];
                        aktualniOtazka = 24;
                    }

                    if (x == 25)
                    {
                        otazka = otazky2Kolo[25];
                        aktualniOtazka = 25;
                    }

                    if (x == 26)
                    {
                        otazka = otazky2Kolo[26];
                        aktualniOtazka = 26;
                    }

                    if (x == 27)
                    {
                        otazka = otazky2Kolo[27];
                        aktualniOtazka = 27;
                    }

                    if (x == 28)
                    {
                        otazka = otazky2Kolo[28];
                        aktualniOtazka = 28;
                    }

                    if (x == 29)
                    {
                        otazka = otazky2Kolo[29];
                        aktualniOtazka = 29;
                    }

                    if (x == 30)
                    {
                        otazka = otazky2Kolo[30];
                        aktualniOtazka = 30;
                    }

                    if (x == 31)
                    {
                        otazka = otazky2Kolo[31];
                        aktualniOtazka = 31;
                    }

                    if (x == 32)
                    {
                        otazka = otazky2Kolo[32];
                        aktualniOtazka = 32;
                    }

                    if (x == 33)
                    {
                        otazka = otazky2Kolo[33];
                        aktualniOtazka = 33;
                    }

                    if (x == 34)
                    {
                        otazka = otazky2Kolo[34];
                        aktualniOtazka = 34;
                    }

                    if (x == 35)
                    {
                        otazka = otazky2Kolo[35];
                        aktualniOtazka = 35;
                    }
                }

                if (kolo == 3)
                {
                    panelCas.Visible = false;

                    aktualniKolo = 3;

                    if (x == 0)
                    {
                        otazka = otazky3Kolo[0];
                        aktualniOtazka = 0;
                    }

                    if (x == 1)
                    {
                        otazka = otazky3Kolo[1];
                        aktualniOtazka = 1;
                    }

                    if (x == 2)
                    {
                        otazka = otazky3Kolo[2];
                        aktualniOtazka = 2;
                    }

                    if (x == 3)
                    {
                        otazka = otazky3Kolo[3];
                        aktualniOtazka = 3;
                    }

                    if (x == 4)
                    {
                        otazka = otazky3Kolo[4];
                        aktualniOtazka = 4;
                    }

                    if (x == 5)
                    {
                        otazka = otazky3Kolo[5];
                        aktualniOtazka = 5;
                    }
                }                
            }
            else
            {
                if (aktualniKolo == 1)
                {
                    lOtazka.Text = odpovedi1Kolo[aktualniOtazka];
                }

                if (aktualniKolo == 2)
                {
                    lOtazka.Text = odpovedi2Kolo[aktualniOtazka];
                }

                if (aktualniKolo == 3)
                {
                    lOtazka.Text = odpovedi3Kolo[aktualniOtazka];
                }
            }

            return otazka;
        }

        private void tretiKolo(int x)
        {
            panelKolo3.Visible = false;
            panelOtazka.Visible = true;
            lOtazka.Visible = true;

            lOtazka.Text = priradOtazku(x, 0, 3);

            if (lOtazka.Text == "ZLATÁ CIHLIČKA")
            {
                SoundPlayer simpleSound = new SoundPlayer("win1.wav");
                simpleSound.Play();
            }

            pocetOtazek3Kolo++;
        }

        private void druheKolo(int x)
        {
            panelKolo2.Visible = false;
            panelOtazka.Visible = true;
            lOtazka.Visible = true;

            lOtazka.Text = priradOtazku(x, 0, 2);

            if (lOtazka.Text == "STŘÍBRNÁ CIHLIČKA")
            {
                SoundPlayer simpleSound = new SoundPlayer("win1.wav");
                simpleSound.Play();
            }

            pocetOtazek2Kolo++;
        }

        private void prvniKoloSloupecek(int x, int y)
        {
            switch (x)
            {
                case 0: prvniKoloRadek1(y);
                    break;
                case 1: prvniKoloRadek2(y);
                    break;
                case 2: prvniKoloRadek3(y);
                    break;
                case 3: prvniKoloRadek4(y);
                    break;
                case 4: prvniKoloRadek5(y);
                    break;
                case 5: prvniKoloRadek6(y);
                    break;
            }

            panelKolo1.Visible = false;
            panelOtazka.Visible = true;
            lOtazka.Visible = true;

            lOtazka.Text = priradOtazku(x, y, 1);

            if (lOtazka.Text == "BRONZOVÁ CIHLIČKA")
            {
                SoundPlayer simpleSound = new SoundPlayer("win1.wav");
                simpleSound.Play();
            }

            pocetOtazek1Kolo++;
        }

        private void btnTema1Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema11000.Enabled == true) test = false;
            if (btnTema12000.Enabled == true) test = false;
            if (btnTema13000.Enabled == true) test = false;
            if (btnTema14000.Enabled == true) test = false;
            if (btnTema15000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(0, 0);
                Castka = 5000;
            }
        }

        private void btnTema11000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(0, 1);
            Castka = 1000;
        }


        private void btnTema12000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(0, 2);
            Castka = 2000;
        }

        private void btnTema13000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(0, 3);
            Castka = 3000;
        }

        private void btnTema14000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(0, 4);
            Castka = 4000;
        }

        private void btnTema15000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(0, 5);
            Castka = 5000;
        }

        private void btnTema2Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema21000.Enabled == true) test = false;
            if (btnTema22000.Enabled == true) test = false;
            if (btnTema23000.Enabled == true) test = false;
            if (btnTema24000.Enabled == true) test = false;
            if (btnTema25000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(1, 0);
                Castka = 5000;
            }
        }

        private void btnTema21000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(1, 1);
            Castka = 1000;
        }

        private void btnTema22000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(1, 2);
            Castka = 2000;
        }

        private void btnTema23000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(1, 3);
            Castka = 3000;
        }

        private void btnTema24000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(1, 4);
            Castka = 4000;
        }

        private void btnTema25000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(1, 5);
            Castka = 5000;
        }

        private void btnTema3Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema31000.Enabled == true) test = false;
            if (btnTema32000.Enabled == true) test = false;
            if (btnTema33000.Enabled == true) test = false;
            if (btnTema34000.Enabled == true) test = false;
            if (btnTema35000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(2, 0);
                Castka = 5000;
            }
        }

        private void btnTema31000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(2, 1);
            Castka = 1000;
        }

        private void btnTema32000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(2, 2);
            Castka = 2000;
        }

        private void btnTema33000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(2, 3);
            Castka = 3000;
        }

        private void btnTema34000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(2, 4);
            Castka = 4000;
        }

        private void btnTema35000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(2, 5);
            Castka = 5000;
        }

        private void btnTema4Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema41000.Enabled == true) test = false;
            if (btnTema42000.Enabled == true) test = false;
            if (btnTema43000.Enabled == true) test = false;
            if (btnTema44000.Enabled == true) test = false;
            if (btnTema45000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(3, 0);
                Castka = 5000;
            }
        }

        private void btnTema41000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(3, 1);
            Castka = 1000;
        }

        private void btnTema42000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(3, 2);
            Castka = 2000;
        }

        private void btnTema43000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(3, 3);
            Castka = 3000;
        }

        private void btnTema44000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(3, 4);
            Castka = 4000;
        }

        private void btnTema45000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(3, 5);
            Castka = 5000;
        }

        private void btnTema5Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema51000.Enabled == true) test = false;
            if (btnTema52000.Enabled == true) test = false;
            if (btnTema53000.Enabled == true) test = false;
            if (btnTema54000.Enabled == true) test = false;
            if (btnTema55000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(4, 0);
                Castka = 5000;
            }
        }

        private void btnTema51000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(4, 1);
            Castka = 1000;
        }

        private void btnTema52000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(4, 2);
            Castka = 2000;
        }

        private void btnTema53000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(4, 3);
            Castka = 3000;
        }

        private void btnTema54000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(4, 4);
            Castka = 4000;
        }

        private void btnTema55000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(4, 5);
            Castka = 5000;
        }

        private void btnTema6Bonus_Click(object sender, EventArgs e)
        {
            bool test = true;

            if (btnTema61000.Enabled == true) test = false;
            if (btnTema62000.Enabled == true) test = false;
            if (btnTema63000.Enabled == true) test = false;
            if (btnTema64000.Enabled == true) test = false;
            if (btnTema65000.Enabled == true) test = false;

            if (test)
            {
                prvniKoloSloupecek(5, 0);
                Castka = 5000;
            }
        }

        private void btnTema61000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(5, 1);
            Castka = 1000;
        }

        private void btnTema62000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(5, 2);
            Castka = 2000;
        }

        private void btnTema63000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(5, 3);
            Castka = 3000;
        }

        private void btnTema64000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(5, 4);
            Castka = 4000;
        }

        private void btnTema65000_Click(object sender, EventArgs e)
        {
            prvniKoloSloupecek(5, 5);
            Castka = 5000;
        }

        private void zobrazDruheKolo()
        {
            panelKolo3.Visible = false;
            panelKolo2.Visible = true;

            btn2Kolo1.Visible = true;
            btn2Kolo2.Visible = true;
            btn2Kolo3.Visible = true;
            btn2Kolo4.Visible = true;
            btn2Kolo5.Visible = true;
            btn2Kolo6.Visible = true;
            btn2Kolo7.Visible = true;
            btn2Kolo8.Visible = true;
            btn2Kolo9.Visible = true;
            btn2Kolo10.Visible = true;
            btn2Kolo11.Visible = true;
            btn2Kolo12.Visible = true;
            btn2Kolo13.Visible = true;
            btn2Kolo14.Visible = true;
            btn2Kolo15.Visible = true;
            btn2Kolo16.Visible = true;
            btn2Kolo17.Visible = true;
            btn2Kolo18.Visible = true;
            btn2Kolo19.Visible = true;
            btn2Kolo20.Visible = true;
            btn2Kolo21.Visible = true;
            btn2Kolo22.Visible = true;
            btn2Kolo23.Visible = true;
            btn2Kolo24.Visible = true;
            btn2Kolo25.Visible = true;
            btn2Kolo26.Visible = true;
            btn2Kolo27.Visible = true;
            btn2Kolo28.Visible = true;
            btn2Kolo29.Visible = true;
            btn2Kolo30.Visible = true;
            btn2Kolo31.Visible = true;
            btn2Kolo32.Visible = true;
            btn2Kolo33.Visible = true;
            btn2Kolo34.Visible = true;
            btn2Kolo35.Visible = true;
            btn2Kolo36.Visible = true;
        }

        private void otazkaAodpoved()
        {
            bool druheKolo = false;
            bool tretiKolo = false;

            if (pocetOtazek1Kolo < 36)
            {
                panelKolo1.Visible = true;
                druheKolo = false;
            }
            else
            {
                if (!JizSpusteneDruheKolo)
                {
                    JizSpusteneDruheKolo = true;
                    stopky.Start();

                    zobrazDruheKolo();
                    druheKolo = true;
                }
                else
                {
                    if (pocetOtazek2Kolo < 36)
                    {
                        zobrazDruheKolo();
                        druheKolo = true;
                    }
                    else
                    {
                        if (!JizSpusteneTretiKolo)
                        {
                            stopky.Stop();
                            panelKolo2.Visible = false;
                            panelOtazka.Visible = false;
                            panelKolo3.Visible = true;

                            JizSpusteneTretiKolo = true;

                            tretiKolo = true;
                        }
                    }
                }

                if (JizSpusteneTretiKolo)
                {
                    if (pocetOtazek3Kolo < 3)
                    {
                        panelOtazka.Visible = false;
                        panelKolo3.Visible = true;

                        tretiKolo = true;
                    }
                    else
                    {
                        panelOtazka.Visible = false;
                        panelKolo3.Visible = false;
                        panelKonec.Visible = true;
                        panelKolo1.Visible = false;
                        panelKolo2.Visible = false;
                        lKonec.Visible = true;
                    }
                }
            }

            //formVysledky.GetSetCastka = Castka;
            //formVysledky.GetSetDruheKolo = druheKolo;
            //formVysledky.GetSetTretiKolo = tretiKolo;
            //formVysledky.GetSetCas = CasDruheKolo;
            //formVysledky.Focus();

            if (JizSpusteneDruheKolo)
            {
                if (stopky.Elapsed.Minutes >= CasDruheKolo)
                {
                    stopky.Stop();
                    panelKolo2.Visible = false;
                    panelOtazka.Visible = false;
                    panelKolo3.Visible = true;

                    JizSpusteneTretiKolo = true;

                    tretiKolo = true;
                }
            }

            formVysledky.GetSetCastka = Castka;
            formVysledky.GetSetDruheKolo = druheKolo;
            formVysledky.GetSetTretiKolo = tretiKolo;
            formVysledky.GetSetCas = CasDruheKolo;
            formVysledky.Focus();
        }

        private void lOtazka_Click(object sender, EventArgs e)
        {
            if (OtazkaNeboOdpoved) OtazkaNeboOdpoved = false;
            else OtazkaNeboOdpoved = true;

            if (OtazkaNeboOdpoved)
            {
                otazkaAodpoved();
                lPrihlasenyTym.Text = "Přihlášený tým: ";

                //OtazkaNeboOdpoved = false;
            }
            else
            {
                priradOtazku(-1, -1, 0);

                //OtazkaNeboOdpoved = true;
            }
            
            nezachytavat = false;
            stisknuto.Clear();
            zakazano.Clear();

            reloadPicture();
            timerOdpoved.Enabled = false;
        }

        private void zpracujOtazky(string radek, int pozice)
        {
            if (pozice == 1) CasDruheKolo = Convert.ToInt32(radek);
            if (pozice == 2) btnTema1Bonus.Text = radek;
            if (pozice == 9) btnTema2Bonus.Text = radek;
            if (pozice == 16) btnTema3Bonus.Text = radek;
            if (pozice == 23) btnTema4Bonus.Text = radek;
            if (pozice == 30) btnTema5Bonus.Text = radek;
            if (pozice == 37) btnTema6Bonus.Text = radek;

            if (pozice > 1 && pozice <= 43)
            {
                if (pozice != 2 && pozice != 9 && pozice != 16 && pozice != 23 && pozice != 30 && pozice != 37)
                {
                    bool zmena = false;

                    string otazka = "";
                    string odpoved = "";

                    for (int i = 0; i < radek.Length; i++)
                    {
                        if (radek[i].ToString() == "|") zmena = true;
                        else
                        {
                            if (!zmena)
                            {
                                if (radek[i].ToString() == "#") otazka = otazka + (char)13;
                                else otazka = otazka + radek[i];
                            }
                            else odpoved = odpoved + radek[i];
                        }
                    }

                    otazky1Kolo.Add(otazka);
                    odpovedi1Kolo.Add(odpoved);
                }
            }

            if (pozice >= 44 && pozice <= 79)
            {
                bool zmena = false;

                string otazka = "";
                string odpoved = "";

                for (int i = 0; i < radek.Length; i++)
                {
                    if (radek[i].ToString() == "|") zmena = true;
                    else
                    {
                        if (!zmena)
                        {
                            if (radek[i].ToString() == "#") otazka = otazka + (char)13;
                            else otazka = otazka + radek[i];
                        }
                        else odpoved = odpoved + radek[i];
                    }
                }

                otazky2Kolo.Add(otazka);
                odpovedi2Kolo.Add(odpoved);
            }

            if (pozice >= 80 && pozice <= 85)
            {
                bool zmena = false;

                string otazka = "";
                string odpoved = "";

                for (int i = 0; i < radek.Length; i++)
                {
                    if (radek[i].ToString() == "|") zmena = true;
                    else
                    {
                        if (!zmena)
                        {
                            if (radek[i].ToString() == "#") otazka = otazka + (char)13;
                            else otazka = otazka + radek[i];
                        }
                        else odpoved = odpoved + radek[i];
                    }
                }

                otazky3Kolo.Add(otazka);
                odpovedi3Kolo.Add(odpoved);
            }
        }

        private void zpracujSoubor(string soubor)
        {
            int radekCislo = 0;
            string radek = "";

            try
            {
                using (StreamReader sr = File.OpenText(soubor))
                {
                    for (int i = 0; i < 85; i++)
                    {
                        radek = sr.ReadLine();
                        radekCislo++;

                        zpracujOtazky(radek, radekCislo);
                    }
                }
            }
            catch
            {

            }
        }

        private void frmHlavni_Load(object sender, EventArgs e)
        {
            m_sp.AutoDetectAndOpen();

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                zpracujSoubor(openFileDialog.FileName);

                nezachytavat = false;
                casOdpovedi = 0;
                stisknuto.Clear();
                zakazano.Clear();

            }
        }

        private void frmHlavni_Activated(object sender, EventArgs e)
        {

        }

        private void frmHlavni_Shown(object sender, EventArgs e)
        {
            formVysledky.Show();
        }

        private void btn2Kolo1_Click(object sender, EventArgs e)
        {
            druheKolo(0);
            Castka = 1000;
            btn2Kolo1.Enabled = false;
        }

        private void btn2Kolo2_Click(object sender, EventArgs e)
        {
            druheKolo(1);
            Castka = 2000;
            btn2Kolo2.Enabled = false;
        }

        private void btn2Kolo3_Click(object sender, EventArgs e)
        {
            druheKolo(2);
            Castka = 3000;
            btn2Kolo3.Enabled = false;
        }

        private void btn2Kolo4_Click(object sender, EventArgs e)
        {
            druheKolo(3);
            Castka = 5000;
            btn2Kolo4.Enabled = false;
        }

        private void btn2Kolo5_Click(object sender, EventArgs e)
        {
            druheKolo(4);
            Castka = 8000;
            btn2Kolo5.Enabled = false;
        }

        private void btn2Kolo6_Click(object sender, EventArgs e)
        {
            druheKolo(5);
            Castka = 10000;
            btn2Kolo6.Enabled = false;
        }

        private void btn2Kolo7_Click(object sender, EventArgs e)
        {
            druheKolo(6);
            Castka = 11000;
            btn2Kolo7.Enabled = false;
        }

        private void btn2Kolo8_Click(object sender, EventArgs e)
        {
            druheKolo(7);
            Castka = 12000;
            btn2Kolo8.Enabled = false;
        }

        private void btn2Kolo9_Click(object sender, EventArgs e)
        {
            druheKolo(8);
            Castka = 13000;
            btn2Kolo9.Enabled = false;
        }

        private void btn2Kolo10_Click(object sender, EventArgs e)
        {
            druheKolo(9);
            Castka = 15000;
            btn2Kolo10.Enabled = false;
        }

        private void btn2Kolo11_Click(object sender, EventArgs e)
        {
            druheKolo(10);
            Castka = 18000;
            btn2Kolo11.Enabled = false;
        }

        private void btn2Kolo12_Click(object sender, EventArgs e)
        {
            druheKolo(11);
            Castka = 20000;
            btn2Kolo12.Enabled = false;
        }

        private void btn2Kolo13_Click(object sender, EventArgs e)
        {
            druheKolo(12);
            Castka = 21000;
            btn2Kolo13.Enabled = false;
        }

        private void btn2Kolo14_Click(object sender, EventArgs e)
        {
            druheKolo(13);
            Castka = 22000;
            btn2Kolo14.Enabled = false;
        }

        private void btn2Kolo15_Click(object sender, EventArgs e)
        {
            druheKolo(14);
            Castka = 23000;
            btn2Kolo15.Enabled = false;
        }

        private void btn2Kolo16_Click(object sender, EventArgs e)
        {
            druheKolo(15);
            Castka = 25000;
            btn2Kolo16.Enabled = false;
        }

        private void btn2Kolo17_Click(object sender, EventArgs e)
        {
            druheKolo(16);
            Castka = 28000;
            btn2Kolo17.Enabled = false;
        }

        private void btn2Kolo18_Click(object sender, EventArgs e)
        {
            druheKolo(17);
            Castka = 30000;
            btn2Kolo18.Enabled = false;
        }

        private void btn2Kolo19_Click(object sender, EventArgs e)
        {
            druheKolo(18);
            Castka = 31000;
            btn2Kolo19.Enabled = false;
        }

        private void btn2Kolo20_Click(object sender, EventArgs e)
        {
            druheKolo(19);
            Castka = 32000;
            btn2Kolo20.Enabled = false;
        }

        private void btn2Kolo21_Click(object sender, EventArgs e)
        {
            druheKolo(20);
            Castka = 33000;
            btn2Kolo21.Enabled = false;
        }

        private void btn2Kolo22_Click(object sender, EventArgs e)
        {
            druheKolo(21);
            Castka = 35000;
            btn2Kolo22.Enabled = false;
        }

        private void btn2Kolo23_Click(object sender, EventArgs e)
        {
            druheKolo(22);
            Castka = 38000;
            btn2Kolo23.Enabled = false;
        }

        private void btn2Kolo24_Click(object sender, EventArgs e)
        {
            druheKolo(23);
            Castka = 40000;
            btn2Kolo24.Enabled = false;
        }

        private void btn2Kolo25_Click(object sender, EventArgs e)
        {
            druheKolo(24);
            Castka = 41000;
            btn2Kolo25.Enabled = false;
        }

        private void btn2Kolo26_Click(object sender, EventArgs e)
        {
            druheKolo(25);
            Castka = 42000;
            btn2Kolo26.Enabled = false;
        }

        private void btn2Kolo27_Click(object sender, EventArgs e)
        {
            druheKolo(26);
            Castka = 43000;
            btn2Kolo27.Enabled = false;
        }

        private void btn2Kolo28_Click(object sender, EventArgs e)
        {
            druheKolo(27);
            Castka = 45000;
            btn2Kolo28.Enabled = false;
        }

        private void btn2Kolo29_Click(object sender, EventArgs e)
        {
            druheKolo(28);
            Castka = 48000;
            btn2Kolo29.Enabled = false;
        }

        private void btn2Kolo30_Click(object sender, EventArgs e)
        {
            druheKolo(29);
            Castka = 50000;
            btn2Kolo30.Enabled = false;
        }

        private void btn2Kolo31_Click(object sender, EventArgs e)
        {
            druheKolo(30);
            Castka = 51000;
            btn2Kolo31.Enabled = false;
        }

        private void btn2Kolo32_Click(object sender, EventArgs e)
        {
            druheKolo(31);
            Castka = 52000;
            btn2Kolo32.Enabled = false;
        }

        private void btn2Kolo33_Click(object sender, EventArgs e)
        {
            druheKolo(32);
            Castka = 53000;
            btn2Kolo33.Enabled = false;
        }

        private void btn2Kolo34_Click(object sender, EventArgs e)
        {
            druheKolo(33);
            Castka = 55000;
            btn2Kolo34.Enabled = false;
        }

        private void btn2Kolo35_Click(object sender, EventArgs e)
        {
            druheKolo(34);
            Castka = 58000;
            btn2Kolo35.Enabled = false;
        }

        private void btn2Kolo36_Click(object sender, EventArgs e)
        {
            druheKolo(35);
            Castka = 60000;
            btn2Kolo36.Enabled = false;
        }

        private void btnKoruna_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(0);
            Castka = formCastka.GetSetCastka;
            btnKoruna.Enabled = false;
        }

        private void btnDolar_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(1);
            Castka = formCastka.GetSetCastka;
            btnDolar.Enabled = false;
        }

        private void btnLibra_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(2);
            Castka = formCastka.GetSetCastka;
            btnLibra.Enabled = false;
        }

        private void btnEuro_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(3);
            Castka = formCastka.GetSetCastka;
            btnEuro.Enabled = false;
        }

        private void btnRupie_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(4);
            Castka = formCastka.GetSetCastka;
            btnRupie.Enabled = false;
        }

        private void btnRubl_Click(object sender, EventArgs e)
        {
            frmCastka formCastka = new frmCastka();

            formCastka.ShowDialog();

            tretiKolo(5);
            Castka = formCastka.GetSetCastka;
            btnRubl.Enabled = false;
        }

        private void lPrihlasenyTym_Click(object sender, EventArgs e)
        {
            stisknuto.Clear();
            nezachytavat = false;
            lPrihlasenyTym.Text = "Přihlášený tým: ";
            timerOdpoved.Enabled = false;

            SoundPlayer simpleSound = new SoundPlayer("eff1.wav");
            simpleSound.Play();

            reloadPicture();

            formVysledky.GetSetCastka = Castka;
            formVysledky.Focus();
        }

        private void reloadPicture()
        {
            picture1.Load("02.png");
            picture2.Load("02.png");
            picture3.Load("02.png");
            picture4.Load("02.png");
            picture5.Load("02.png");
            picture6.Load("02.png");
            picture7.Load("02.png");
            picture8.Load("02.png");
            picture9.Load("02.png");
            picture10.Load("02.png");
            picture11.Load("02.png");
            picture12.Load("02.png");
        }

        private void timerOdpoved_Tick(object sender, EventArgs e)
        {
            casOdpovedi++;

            switch (casOdpovedi)
            {
                case 1: picture1.Load("01.png");
                    picture12.Load("01.png");
                    break;
                case 2:
                    picture2.Load("01.png");
                    picture11.Load("01.png");
                    break;
                case 3:
                    picture3.Load("01.png");
                    picture10.Load("01.png");
                    break;
                case 4:
                    picture4.Load("01.png");
                    picture9.Load("01.png");
                    break;
                case 5:
                    picture5.Load("01.png");
                    picture8.Load("01.png");
                    break;
                case 6:
                    picture6.Load("01.png");
                    picture7.Load("01.png");

                    timerOdpoved.Enabled = false;

                    SoundPlayer simpleSound = new SoundPlayer("eff1.wav");
                    simpleSound.Play();

                    //System.Threading.Thread.Sleep(1000);

                    stisknuto.Clear();
                    nezachytavat = false;
                    lPrihlasenyTym.Text = "Přihlášený tým: ";

                    reloadPicture();

                    formVysledky.GetSetCastka = Castka;
                    formVysledky.Focus();

                    break;
            }
        }

        private void frmHlavni_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_sp.Close();
        }
    }
}
