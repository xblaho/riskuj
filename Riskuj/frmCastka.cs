﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Riskuj
{
    public partial class frmCastka : Form
    {
        public frmCastka()
        {
            InitializeComponent();
        }

        private int Castka = 0;

        public int GetSetCastka
        {
            get { return Castka; }
            set { Castka = value; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Castka = Convert.ToInt32(nmCastka.Value);

            Close();
        }

        private void frmCastka_Shown(object sender, EventArgs e)
        {
            nmCastka.Value = Castka;

            nmCastka.Focus();
        }

        private void nmCastka_Enter(object sender, EventArgs e)
        {
            string ret = Castka.ToString();

            nmCastka.Select(0, ret.Length);
        }

        private void nmCastka_ValueChanged(object sender, EventArgs e)
        {
            string ret = Castka.ToString();

            nmCastka.Select(0, ret.Length);
        }
    }
}
